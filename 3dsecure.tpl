<div class="page-content page-app page-profil" style="margin-top:21px; ">
  <div class="col-lg-10 col-md-9">
    {include file="$template/header_client.tpl"}
	    <div class="profil-content" >
        <div class="row"  style="padding-left: 20px;">

{include file="$template/includes/alert.tpl" type="info" msg=$LANG.creditcard3dsecure textcenter=true}

<br /><br />

<div class="text-center">

    <div id="frmThreeDAuth" class="hidden">
        {$code}
    </div>

    <iframe name="3dauth" height="500" scrolling="auto" src="about:blank" class="submit-3d"></iframe>

    <br /><br />

</div>

<script language="javascript">
    jQuery("#frmThreeDAuth").find("form:first").attr('target', '3dauth');
    setTimeout("autoSubmitFormByContainer('frmThreeDAuth')", 1000);
</script>
</div>

</div>
{include file="$template/sidebar_client.tpl"}</div>