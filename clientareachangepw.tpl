<div class="page-content" style="margin-top: 21px;">
	<div class="header">
		<h2><strong>Change</strong> Password</h2>
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				{foreach $breadcrumb as $item}
				<li{if $item@last} class="active"{/if}>
				{if !$item@last}<a href="{$item.link}">{/if}
				{$item.label}
				{if !$item@last}</a>{/if}
			</li>
			{/foreach}
		</ol>
	</div>
</div>
<div class="profil-content">
	{if $successful}
	{include file="$template/includes/alert.tpl" type="success" msg=$LANG.changessavedsuccessfully textcenter=true}
	{/if}
	{if $errormessage}
	{include file="$template/includes/alert.tpl" type="error" errorshtml=$errormessage}
	{/if}
	<div class=" col-md-offset-3 col-md-6">
		<div class="panel">
			<div class="panel-content">
				<form class="form-horizontal using-password-strength" method="post" action="clientarea.php?action=changepw" role="form">
					<input type="hidden" name="submit" value="true" />
					<div class="form-group">
						<label for="inputExistingPassword" class="col-sm-3 control-label">{$LANG.existingpassword}</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" name="existingpw" id="inputExistingPassword" />
						</div>
					</div>
					<div id="newPassword1" class="form-group has-feedback">
						<label for="inputNewPassword1" class="col-sm-3 control-label">{$LANG.newpassword}</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" name="newpw" id="inputNewPassword1" />
							<span class="form-control-feedback glyphicon"></span>
							{include file="$template/includes/pwstrength.tpl"}
						</div>
					</div>
					<div id="newPassword2" class="form-group has-feedback">
						<label for="inputNewPassword2" class="col-sm-3 control-label">{$LANG.confirmnewpassword}</label>
						<div class="col-sm-9">
							<input type="password" class="form-control" name="confirmpw" id="inputNewPassword2" />
							<span class="form-control-feedback glyphicon"></span>
							<div id="inputNewPassword2Msg"></div>
						</div>
					</div>
					<div class="form-group">
						<div class="text-center">
							<input class="btn btn-primary" type="submit" value="{$LANG.clientareasavechanges}" />
							<input class="btn btn-default" type="reset" value="{$LANG.cancel}" />
						</div>
					</div>
				</form>
			</div>
		</div>			
	</div>
	<div class="col-md-6 col-md-offset-3">
		<div class="panel">
			<div class="panel-header">
				<h3><strong>Two</strong> Factor Auth</h3>
			</div>
			<div class="panel-content">
				<p>To enhance your account security we strongly suggest you activate two factor authentication.</p>
				<a href="/clientarea.php?action=security">
					<button type="button" class="btn btn-block btn-primary bd-0 no-bd"><i class="fa  fa-key"></i> &nbsp;Security Settings</button>
				</a>
			</div>
		</div>
	</div>
</div>
</div>