<div class="page-content page-app page-profil" style="margin-top: 21px;">
  <div class="col-lg-10 col-md-9">
		{include file="$template/header_client.tpl"}
<div class="profil-content">

<h3>{$LANG.domaingeteppcode}</h3>

<p>
    {$LANG.domaingeteppcodeexplanation}
</p>

<br />

{if $error}
    {include file="$template/includes/alert.tpl" type="error" msg=$LANG.domaingeteppcodefailure|cat:" $error"}
{elseif $eppcode}
    {include file="$template/includes/alert.tpl" type="warning" msg=$LANG.domaingeteppcodeis|cat:" $eppcode"}
{else}
    {include file="$template/includes/alert.tpl" type="warning" msg=$LANG.domaingeteppcodeemailconfirmation}
{/if}
</div>
</div>
  {include file="$template/sidebar_client.tpl"}
</div>