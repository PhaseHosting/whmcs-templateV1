<!DOCTYPE html>

<style>
.btn-midnight-blue {
    background-color: #18a689;
    color: #fff;
}
</style>
{function name=outputHomePanels}

<div menuItemName="{$item->getName()}" class=""{if $item->getAttribute('id')} id="{$item->getAttribute('id')}"{/if} {if $item->getName() == 'Recent News'}hidden{/if}>
  <div class="panel {if $item->getName() == 'Domains Expiring Soon' || $item->getName() == 'Unpaid Invoices' || $item->getName() == 'Overdue Invoices'}bg-red{/if}">
    <div class="panel-header header-line">
     <h3><a href="#" class="panel-toggle {if $item->getName() == 'Recent Support Tickets' || $item->getName() == 'Register a New Domain' || $item->getName() == 'Recent News'}closed{/if}" style="{if $item->getName() == 'Domains Expiring Soon' || $item->getName() == 'Unpaid Invoices' || $item->getName() == 'Overdue Invoices'}color:#fff{else}color:#5b5b5b{/if}; text-decoration:none;">
      <!-- {if $item->hasIcon()}<i class="{$item->getIcon()}"></i>&nbsp;{/if} -->
      {$item->getLabel()}
      <!-- {if $item->hasBadge()}&nbsp;<span class="badge">{$item->getBadge()}</span>{/if} -->
      </a>

    </h3>
    <div class="control-btn">
      
            <a href="#" class="panel-toggle {if $item->getName() == 'Recent Support Tickets' || $item->getName() == 'Register a New Domain' || $item->getName() == 'Recent News'}closed{/if}"><i class="fa fa-angle-down"></i></a>
        </div>
  </div>
  <div class="panel-content" {if $item->getName() == 'Register a New Domain' || $item->getName() == 'Recent News'}style="display: none;"{/if}>
    {if $item->hasBodyHtml()}
    <div class="">
      {$item->getBodyHtml()}
    </div>
    {/if}
    {if $item->hasChildren()}
    <div class="list-group{if $item->getChildrenAttribute('class')} {$item->getChildrenAttribute('class')}{/if}">
      {foreach $item->getChildren() as $childItem}
      {if $childItem->getUri()}
      <a menuItemName="{$childItem->getName()}" href="{$childItem->getUri()}" class="list-group-item{if $childItem->getClass()} {$childItem->getClass()}{/if}{if $childItem->isCurrent()} active{/if}"{if $childItem->getAttribute('dataToggleTab')} data-toggle="tab"{/if}{if $childItem->getAttribute('target')} target="{$childItem->getAttribute('target')}"{/if} id="{$childItem->getId()}" style="border:0px;">
        {if $childItem->hasIcon()}<i class="{$childItem->getIcon()}"></i>&nbsp;{/if}
        {$childItem->getLabel()}
        {if $childItem->hasBadge()}&nbsp;<span class="badge">{$childItem->getBadge()}</span>{/if}
      </a>
      <hr/>
      {else}
      <div menuItemName="{$childItem->getName()}" class="list-group-item{if $childItem->getClass()} {$childItem->getClass()}{/if}" id="{$childItem->getId()}">
        {if $childItem->hasIcon()}<i class="{$childItem->getIcon()}"></i>&nbsp;{/if}
        {$childItem->getLabel()}
        {if $childItem->hasBadge()}&nbsp;<span class="badge">{$childItem->getBadge()}</span>{/if}
      </div>
      {/if}
      {/foreach}
    </div>
    {/if}
    
    {if $item->hasFooterHtml()}
    {$item->getFooterHtml()}
    {/if}
  </div>
  {if $item->getExtra('btn-link') && $item->getExtra('btn-text')}
      <div class="panel-footer clearfix text-right {if $item->getName() == 'Domains Expiring Soon' || $item->getName() == 'Unpaid Invoices' || $item->getName() == 'Overdue Invoices'}bg-red{else} {/if}">

      
        <button data-rel="tooltip" type="button" onclick="window.location.href='/{$item->getExtra('btn-link')}'" class="btn btn-sm btn-icon btn-rounded {if $item->getName() == 'Domains Expiring Soon' || $item->getName() == 'Unpaid Invoices' || $item->getName() == 'Overdue Invoices'}btn-dark{else}btn-primary{/if}" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{$item->getExtra('btn-text')}">
          {if $item->getExtra('btn-icon')}<i class="fa {$item->getExtra('btn-icon')}"></i>{/if}
          
        
      </form>
        

      </div>
      {/if}
</div>
</div>
{/function}

{if $clientsstats.productsnumtotal != 0 || $clientsstats.numactivedomains != 0}
<div class="page-content page-app page-profil" style="margin-top: 21px; min-height:94vh">
  <div class="col-lg-10 col-md-9">
    {include file="$template/header_client.tpl"}
    <div class="profil-content">
      {foreach from=$addons_html item=addon_html}
      <div>
        {$addon_html}
      </div>
      {/foreach}
      <div class="client-home-panels">
        <div class="row">
          <div class="col-sm-6">
            {foreach $panels as $item}
            {if $item@iteration is odd}
            {outputHomePanels}
            {/if}
            {/foreach}
          </div>
          <div class="col-sm-6">
            <div class="col-sm-12">
              
              {foreach $panels as $item}
              {if $item@iteration is even}
              {outputHomePanels}
              {/if}
              {/foreach}
            </div>
            <div class="col-sm-12">
              <form role="form" method="post" action="clientarea.php?action=kbsearch">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {include file="$template/sidebar_client.tpl"}
  </div>
  {else}
  <div class="page-content" style="margin-top: 21px;">
    
    
    <div class="header">
      <h2><strong>{$LANG.welcomehome}</strong> {$clientsdetails.firstname}</h2>
      <h2>{$LANG.cartdomainsnohosting}</h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          {foreach $breadcrumb as $item}
          <li{if $item@last} class="active"{/if}>
          {if !$item@last}<a href="{$item.link}">{/if}
            {$item.label}
            {if !$item@last}</a>{/if}
          </li>
          {/foreach}
        </ol>
      </div>
    </div>
    
    <div class="col-md-2">
    </div>
    
    <div class="col-md-2">
      <a href="/cart.php">
        <div class="panel-transparant">
          <div class="panel-content text-center">
            <i class=" fa fa-desktop fa-4x"></i>
            <h3><strong>Web</strong> Hosting</h3>
          </div>
        </div>
      </a>
    </div>
    
    <div class="col-md-2">
      <a href="/cart.php">
        <div class="panel-transparant">
          <div class="panel-content text-center">
            <i class=" fa fa-wordpress fa-4x"></i>
            <h3><strong>WordPress</strong> Hosting</h3>
          </div>
        </div>
      </a>
    </div>
    
    <div class="col-md-2">
      <a href="/cart.php?gid=20">
        <div class="panel-transparant">
          <div class="panel-content text-center">
            <i class=" fa fa-server fa-4x"></i>
            <h3><strong>Ghost</strong> Hosting</h3>
          </div>
        </div>
      </a>
    </div>
    
    <div class="col-md-2">
      <a href="/domainchecker.php">
        <div class="panel-transparant">
          <div class="panel-content text-center">
            <i class=" fa fa-mail-forward fa-4x"></i>
            
            <h3>{$LANG.orderdomainhome}</h3>
          </div>
        </div>
      </a>
      
    </div>
    <div class="col-md-2">
    </div>
    
    {/if}
