<div class="page-content" style="margin-top: 21px;">
	<div class="header">
		<h2><strong>Manage</strong> Services</h2>
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				{foreach $breadcrumb as $item}
				<li{if $item@last} class="active"{/if}>
				{if !$item@last}<a href="{$item.link}">{/if}
				{$item.label}
				{if !$item@last}</a>{/if}
			</li>
			{/foreach}
		</ol>
	</div>
</div>

<div class="profil-content">
	{if $producttype == 'other'}

	{else}
	<div class="col-md-10 col-md-offset-1">
		<div class="col-md-12">
			<!--  -->
			<div class="col-md-12">
				<div class="col-md-3">
					<div class="panel">
						<div class="panel-header">
							<h3>{$LANG.diskSpace}</h3>
						</div>
						<div class="panel-content text-center">

							<input type="text" value="{$diskpercent|substr:0:-1}%" class="dial-usage" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
							<p class="text-center">{$diskusage}MB / {$disklimit}MB</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="panel">
						<div class="panel-header">
							<h3>{$LANG.bandwidth}</h3>
						</div>
						<div class="panel-content text-center">

							<input type="text" value="{$bwpercent|substr:0:-1}%" class="dial-usage" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
							<p class="text-center">{$bwusage}MB / {$bwlimit}MB</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="panel">
						<div class="panel-header">
							<h3>RAM Usage</h3>
						</div>
						<div class="panel-content text-center">

							<input type="text" value="{$hookOutput.0.accountstats.pmem}%" class="dial-usage" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
							<p class="text-center">{$hookOutput.0.accountstats.amem}MB / {$hookOutput.0.accountstats.mmem}MB</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="panel">
						<div class="panel-header">
							<h3>CPU Usage</h3>
						</div>
						<div class="panel-content text-center">

							<input type="text" value="{$hookOutput.0.accountstats.cpu}%" class="dial-usage" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
							<p class="text-center">{$hookOutput.0.accountstats.cpu}%</p>
						</div>
					</div>
				</div>
				<script src="/assets/js/jquery.knob.js"></script>
			</div>
			{/if}
			<div class="col-md-12">
				{if $suspendreason}
				<div class="alert media fade in alert-danger">
					<p><strong>{$LANG.suspendreason}:</strong> {$suspendreason}</p>
				</div>
				{/if}
				{if $modulecustombuttonresult}
				{if $modulecustombuttonresult == "success"}
				{include file="$template/includes/alert.tpl" type="success" msg=$LANG.moduleactionsuccess textcenter=true idname="alertModuleCustomButtonSuccess"}
				{else}
				{include file="$template/includes/alert.tpl" type="error" msg=$LANG.moduleactionfailed|cat:' ':$modulecustombuttonresult textcenter=true idname="alertModuleCustomButtonFailed"}
				{/if}
				{/if}

				{if $pendingcancellation}
				{include file="$template/includes/alert.tpl" type="error" msg=$LANG.cancellationrequestedexplanation textcenter=true idname="alertPendingCancellation"}
				{/if}
				<div class="text-center module-client-area">
					{$moduleclientarea}
				</div>
				
				<div class="col-md-12">
				<div class="panel">
					<div class="panel-header header-line">
						<h3>Details</h3>
						<div class="control-btn">
							<a href="#" class="panel-toggle"><i class="fa fa-angle-down"></i></a>
						</div> 
					</div>
					<div class="panel-content">
						<div class="row">
							{if $username}

							<div class="col-sm-5 text-right">
								<strong>{$LANG.orderdomain}</strong>
							</div>
							<div class="col-sm-7 text-left">
								{$domain} 
							</div>

							<div class="col-sm-5 text-right">
								<strong>{$LANG.serverusername}</strong>
							</div>
							<div class="col-sm-7 text-left">
								{$username}
							</div>
						</div>
						{/if}
						<div class="row">
							<div class="col-sm-5 text-right">
								<strong>{$LANG.servername}</strong>
							</div>
							<div class="col-sm-7 text-left">
								{$serverdata.hostname}
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5 text-right">
								<strong>{$LANG.domainregisternsip}</strong>
							</div>
							<div class="col-sm-7 text-left">
								{$serverdata.ipaddress}
							</div>
						</div>
						{if $serverdata.nameserver1 || $serverdata.nameserver2 || $serverdata.nameserver3 || $serverdata.nameserver4 || $serverdata.nameserver5}
						<div class="row">
							<div class="col-sm-5 text-right">
								<strong>{$LANG.domainnameservers}</strong>
							</div>
							<div class="col-sm-7 text-left">
								{if $serverdata.nameserver1}{$serverdata.nameserver1}<br />{/if}
								{if $serverdata.nameserver2}{$serverdata.nameserver2}<br />{/if}
								{if $serverdata.nameserver3}{$serverdata.nameserver3}<br />{/if}
								{if $serverdata.nameserver4}{$serverdata.nameserver4}<br />{/if}
								{if $serverdata.nameserver5}{$serverdata.nameserver5}<br />{/if}
							</div>
						</div>
						{/if}
						<div class="row">
							<div class="col-sm-5 text-right">
								<strong>RAM Usage</strong>
							</div>
							<div class="col-sm-7 text-left">
								{$hookOutput.0.accountstats.amem}/{$hookOutput.0.accountstats.mmem} MB
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5 text-right">
								<strong>CPU Usage</strong>
							</div>
							<div class="col-sm-7 text-left">
								{$hookOutput.0.accountstats.cpu}%
							</div>
						</div>
					</div>
					
				</div>
			</div>
			{if $showcancelbutton || $packagesupgrade}
				<div class="row">
					{if $packagesupgrade}
					<div class="col-xs-12">
						<a href="upgrade.php?type=package&amp;id={$id}" class="btn btn-block btn-success">{$LANG.upgrade}</a>
					</div>
					<div class="col-xs-12">
						{else}
						<div class="col-xs-12">
							{/if}
							<a href="clientarea.php?action=cancel&amp;id={$id}" class="btn btn-block btn-danger {if $pendingcancellation}disabled{/if}">{if $pendingcancellation}{$LANG.cancellationrequested}{else}{$LANG.clientareacancelrequestbutton}{/if}</a>
						</div>
					</div>
					

				</div>
				{/if}
			</div>
			</div>
				
				<script src="{$BASE_PATH_JS}/jquery.knob.js"></script>
				<script type="text/javascript">
					jQuery(function() {ldelim}
						jQuery(".dial-usage").knob({ldelim}'format':function (v) {ldelim} alert(v); {rdelim}{rdelim});
						{rdelim});
				</script>
				</div> 
			</div>
		</div>
	</div>








