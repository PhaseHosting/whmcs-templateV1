<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <title>{if $kbarticle.title}{$kbarticle.title} - {/if}{$pagetitle} - {$companyname}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta content="" name="description" />
  <meta content="themes-lab" name="author" />
  <link rel="shortcut icon" href="/templates/make/assets/global/images/favicon.png">
  <link href="/templates/make/assets/global/css/style.css" rel="stylesheet">
  <link href="/templates/make/assets/global/css/ui.css" rel="stylesheet">
  <link href="/templates/make/assets/global/plugins/icheck/skins/all.css" rel="stylesheet"/>
  <link href="/templates/make/assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
  <script src="/templates/make/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
  <script src="/templates/make/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
  <script type="text/javascript" src="{$BASE_PATH_JS}/StatesDropdown.js"></script>
</head>
<body class="sidebar-condensed account2 signup" data-page="signup">
  <!-- BEGIN LOGIN BOX -->
  <div class="container" style="width: 900px;" id="login-block">
    <i class="user-img icons-faces-users-03"></i>
    <div class="account-info" style="width:35%;">
      <h1>PhaseHosting</h1>
      <h3>Control &amp; Administration.</h3>
      <ul>
        <li><i class="icon-magic-wand"></i> Web Hosting</li>
        <li><i class="icon-layers"></i> VPS Hosting</li>
        <li><i class="icon-arrow-right"></i> Cloud Storage</li>
        <li><i class="icon-drop"></i> Email Hosting</li>
        <li><i class="icon-doc"></i> WordPress Hosting</li>
        <li><i class="icon-support"></i> Online Support</li>
        <li><i class="icon-cloud-download"></i> Regular updates</li>
      </ul>
    </div>
    <div class="account-form">
      <form class="form-signup using-password-strength" method="post" action="{$smarty.server.PHP_SELF}" role="form">
        {if $registrationDisabled}
        {include file="$template/includes/alert.tpl" type="error" msg=$LANG.registerCreateAccount|cat:' <strong><a href="cart.php" class="alert-link">'|cat:$LANG.registerCreateAccountOrder|cat:'</a></strong>'}
        {/if}
        
        {if $errormessage}
        {include file="$template/includes/alert.tpl" type="error" errorshtml=$errormessage}
        {/if}
        
        {if !$registrationDisabled}
        <input type="hidden" name="register" value="true"/>
        <h3><strong>Create</strong> your account</h3>
        <div class="row">
          <div class="col-sm-6">
            
            <div class="append-icon">
              <input type="text" name="firstname" id="firstname" class="form-control form-white firstname" placeholder="{$LANG.clientareafirstname}" value="{if $clientfirstname|default:FALSE}{$clientfirstname}{else}{/if}" required autofocus>
              <i class="icon-user"></i>
            </div>
            
          </div>
          <div class="col-sm-6">
            
            <div class="append-icon">
              <input type="text" name="lastname" id="lastname" class="form-control form-white lastname" placeholder="{$LANG.clientarealastname}" value="{if $clientlastname|default:FALSE}{$clientlastname}{else}{/if}" required>
              <i class="icon-user"></i>
            </div>
            
          </div>
          <!--hidden start-->
         <!--  <input type="hidden" name="companyname" id="companyname" value="" class="form-control" />
          <input type="hidden" name="address1" id="address1" value="NA" class="form-control" required />
          <input type="hidden" name="address2" id="address2" value="NA" class="form-control"/>
          <input type="hidden" name="city" id="city" value="NA" class="form-control" required />
          <input type="hidden" name="state" id="state" value="NA" class="form-control" required />
          <input type="hidden" name="postcode" id="postcode" value="NA" class="form-control"  />
          <input type="hidden" name="phonenumber" id="phonenumber" value="00-000-000" class="form-control"  />  -->
          
          
          <!--hidden stop-->
        </div>
        
        <div class="append-icon">
          <input type="email" name="email" id="email" class="form-control form-white email" placeholder="{$LANG.clientareaemail}" value="{if $clientemail|default:FALSE}{$clientemail}{else}{/if}" required>
          <i class="icon-envelope"></i>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <select id="country" name="country" class="form-control form-white">
              {foreach $clientcountries as $countryCode => $countryName}
              <option value="{$countryCode}"{if (!$clientcountry && $countryCode eq $defaultCountry) || ($countryCode eq $clientcountry)} selected="selected"{/if}>
                {$countryName}
              </option>
              {/foreach}
            </select>
          </div>
          <div class="col-sm-6">
            {if $currencies}
            <div class="form-group">
              <select id="currency" name="currency" class="form-control form-white">
                {foreach from=$currencies item=curr}
                <option value="{$curr.id}"{if !$smarty.post.currency && $curr.default || $smarty.post.currency eq $curr.id } selected{/if}>{$curr.code}</option>
                {/foreach}
              </select>
            </div>
            {/if}
          </div>
        </div>
        
        <div class="append-icon m-b-10 has-feedback">
          <input type="password" name="password" id="inputNewPassword1" class="form-control form-white password" placeholder="{$LANG.clientareapassword}" required>
          <i class="icon-lock"></i>
          
        </div>
        <div>
          {include file="$template/includes/pwstrength.tpl"}
        </div>
        <div class="append-icon m-b-20 has-feedback">
          <input type="password" name="password2" id="inputNewPassword2" class="form-control form-white password2" placeholder="{$LANG.clientareaconfirmpassword}" required>
          <i class="icon-lock"></i>
          <div id="inputNewPassword2Msg">
          </div>
          
          <div class="terms option-group"><center>
            <label  for="terms" class="m-t-10">
              <input type="checkbox" name="accepttos" id="terms" data-checkbox="icheckbox_square-blue" required/>
              {$LANG.ordertosagreement} <a href="{$tosurl}" target="_blank">{$LANG.ordertos}</a>
            </label>  </center>
          </div>
          
          <div class="m-t-20"><center>
            <button type="submit" class="btn btn-lg btn-dark btn-rounded" data-style="expand-left">{$LANG.clientregistertitle}</button></center>
          </div>
          
        </form>
        <div class="form-footer">
          <div class="clearfix">
            <p class="new-here"><a href="/login.php">Already have an account? Sign In</a></p>
          </div>
        </div>
      </div>
      
    </div>
    <!-- END LOCKSCREEN BOX -->
    <script src="/templates/make/assets/global/plugins/gsap/main-gsap.min.js"></script>
    <script src="/templates/make/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/templates/make/assets/global/plugins/icheck/icheck.min.js"></script>
    <script src="/templates/make/assets/global/plugins/backstretch/backstretch.min.js"></script>
    <script src="/templates/make/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <script src="/templates/make/assets/global/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="/templates/make/assets/global/plugins/jquery-validation/additional-methods.min.js"></script>
    <script src="/templates/make/assets/global/js/plugins.js"></script>
    <!--<script src="/templates/make/assets/global/js/pages/login-v2.js"></script>
    <script src="/templates/make/assets/admin/layout2/js/layout.js"></script>-->
      {/if}
      {literal}
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-72170997-4', 'auto');
		  ga('send', 'pageview');

		</script>
  {/literal}
  </body>
  </html>

