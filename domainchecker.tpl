<!DOCTYPE html>
<div class="page-content">
  <div class="col-lg-12 col-md-12">

   <div class="profil-content" >

      <div class="">
          <div class="">
              <h2><strong>Order</strong> a new domain</h2>
              <h2>
                  {if $productGroup.headline}
                  {$productGroup.headline}
                  {else}
                  {$productGroup.name}
                  {/if}
              </h2>
              {include file="$template/includes/alert.tpl" type="danger" msg="{$LANG.searchtermrequired}" textcenter=true idname="searchTermRequired" additionalClasses="domain-checker-error" hide=true}

              {include file="$template/includes/alert.tpl" type="danger" msg="{$LANG.invalidchars}" textcenter=true idname="invalidChars" additionalClasses="domain-checker-error" hide=true}

              {if $errorMsg}
              {include file="$template/includes/alert.tpl" type="danger" msg=$errorMsg textcenter=true idname="invalidDomainError" additionalClasses="domain-checker-error"}
              {else}
              {include file="$template/includes/alert.tpl" type="danger" msg="{$LANG.unabletolookup}" textcenter=true idname="invalidDomainError" additionalClasses="domain-checker-error" hide=true}
              {/if}



              <div class="domain-checker-container">
                <div class="domain-checker-bg clearfix">
                   <br><br><br>
                   <form id="frmDomainChecker">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
                            <div class="input-group input-lg">
                                <input type="text" class="form-control form-white input-lg" placeholder="{$LANG.findyourdomain}" value="{$domain}" id="inputDomain" />
                                <span class="input-group-btn">
                                    <button type="submit" id="btnCheckAvailability" class="btn btn-primary domain-check-availability btn-lg" style="margin-right: 0px;">{$LANG.search}</button>
                                    {if $bulkdomainsearchenabled}
                                    <a href="domainchecker.php?search=bulk" id="btnBulkOptions" class="btn btn-default domain-check-availability btn-lg"  />{$LANG.bulkoptions}</a>
                                    {/if}
                                </span>
                            </div>
                        </div>
                    </div>

                    {include file="$template/includes/captcha.tpl"}
                </form>
                <br>
                <br>
                <br>
            </div>
        </div>
        <div class="domain-step-options hidden" id="stepBulkOptions">
            <textarea class="form-control" rows="10" cols="60" id="inputBulkDomains"></textarea>
        </div>
        <div class="domain-step-options{if !$performingLookup} hidden{/if}" id="stepResults">
            {include file="$template/domainchecker-results.tpl"}
        </div>



        <div >



            {if $searchResults}
            <div class="panel-transparent">
                <div class="panel-content">
                    <h2>
                        {$LANG.alltldpricing}
                    </h2>
                    
                    <div class="col-md-12 col-lg-12 ">
                        <table class="table table-striped table-framed">
                            <thead>
                                <tr>
                                    <th class="text-center">{$LANG.domaintld}</th>
                                    <th class="text-center">{$LANG.domainminyears}</th>
                                    <th class="text-center">{$LANG.domainsregister}</th>
                                    <th class="text-center">{$LANG.domainstransfer}</th>
                                    <th class="text-center">{$LANG.domainsrenew}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $tldpricelist as $tld}
                                <tr>
                                    <td>{$tld.tld}</td>
                                    <td class="text-center">{$tld.period}</td>
                                    <td class="text-center">{if $tld.register}{$tld.register}{else}{$LANG.domainregnotavailable}{/if}</td>
                                    <td class="text-center">{if $tld.transfer}{$tld.transfer}{else}{$LANG.domainregnotavailable}{/if}</td>
                                    <td class="text-center">{if $tld.renew}{$tld.renew}{else}{$LANG.domainregnotavailable}{/if}</td>
                                </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
{else}

{/if}
{if !$loggedin && $currencies && !$performingLookup}
<div class="currencychooser pull-right clearfix margin-bottom">
    <div class="btn-group" role="group">
        {foreach from=$currencies item=curr}
        <a href="domainchecker.php?currency={$curr.id}" class="btn btn-default{if $currency.id eq $curr.id} active{/if}">
            <img src="{$BASE_PATH_IMG}/flags/{if $curr.code eq "AUD"}au{elseif $curr.code eq "CAD"}ca{elseif $curr.code eq "EUR"}eu{elseif $curr.code eq "GBP"}gb{elseif $curr.code eq "INR"}in{elseif $curr.code eq "JPY"}jp{elseif $curr.code eq "USD"}us{elseif $curr.code eq "ZAR"}za{else}na{/if}.png" border="0" alt="" />
            {$curr.code}
        </a>
        {/foreach}
    </div>
</div>
<div class="clearfix"></div>
{/if}

<script>
    var langSearch = '{$LANG.search}';
    var langAdding = '{$LANG.domaincheckeradding}';
    var langAdded = '{$LANG.domaincheckeradded}';
    var langUnavailable = '{$LANG.domainunavailable}';
    var langBulkPlaceholder = '{$LANG.domaincheckerbulkplaceholder|escape:'quotes'|replace:"\n":'\n'}';
</script>
<script src="templates/{$template}/js/domainchecker.js"></script>

{include file="$template/includes/modal.tpl" name="CheckUnavailable" title="{$LANG.domainchecker.suggestiontakentitle}" content="{$LANG.domainchecker.suggestiontakenmsg}" closeLabel="{$LANG.domainchecker.suggestiontakenchooseanother}"}

{include file="$template/includes/modal.tpl" name="AlreadyInCart" title="{$LANG.domainchecker.alreadyincarttitle}" content="{$LANG.domainchecker.alreadyincartmsg}" submitAction="window.location='cart.php?a=checkout'" submitLabel="{$LANG.domainchecker.alreadyincartcheckoutnow}"}

{include file="$template/includes/modal.tpl" name="AddToCartError" title="{$LANG.genericerror.title}" content="{$LANG.genericerror.msg}"}
</div>
</div>
</div>
</div>
