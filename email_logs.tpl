<!DOCTYPE html>
<link href="/templates/make/assets/global/plugins/maps-amcharts/ammap/ammap.min.css" rel="stylesheet">
<link href="/templates/make/assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">

<!-- BEGIN PAGE CONTENT -->
<div class="page-content" style="margin-top: 21px;">


	<div class="header">
   <h2><button onclick="goBack()" type="button" class="btn btn-sm btn-icon btn-rounded btn-primary" rel="popover" ><i class="fa fa-arrow-left"></i></button> <strong>Email</strong> Logs</h2>
   <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      {foreach $breadcrumb as $item}
      <li{if $item@last} class="active"{/if}>
      {if !$item@last}<a href="{$item.link}">{/if}
      {$item.label}
      {if !$item@last}</a>{/if}
    </li>
    {/foreach}
  </ol>
</div>
<div class="col-lg-12 col-md-12">
  <div class="profil-content">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel">
        <div class="panel-header header-line">
          <h3>{$email} <strong>Logs</strong></h3>
          <div class="control-btn">
          <a data-toggle="modal" data-target="#help"><i class="icon-question"></i></a> 
        </div>
        </div>
        <div class="panel-content">
        <tbody>
            {if $emailLogs.data == 'No logs available'}
            <div class="text-center"><h4>{$emailLogs.data}</h4></div>
            {else}
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Time</th>
                <th>From</th>
                <th>To</th>
                <th>Subject</th>
                <th>Status</th>
              </tr>
            </thead>
            {foreach $emailLogs.data as $log}
              <tr>
                <td>{$log.timestamp|date_format:"%d-%m-%Y"}</td>
                <td>{$log.message.headers.from}</td>
                <td>{$log.message.headers.to}</td>
                <td>{$log.message.headers.subject}</td>
                <td><button type="button" class="btn {if $log.event == 'failed' || $log.event == 'rejected'} btn-danger {elseif $log.event == 'complained'} btn-warning {elseif $log.event == 'accepted'} btn-default {else} btn-success {/if}" style="width:100%">{$log.event}</button></td>
                <td></td>
              </tr>
            {/foreach}
            {/if}
            </tbody>
          </table>

        </div>
        <div class="panel-footer clearfix">
          <div class="pull-right" data-toggle="modal" data-target="#addEmail">
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- HELP -->
<div class="modal fade" id="help" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>Help</strong></h4>
      </div>
      <div class="modal-body">
      <!-- step 01 -->
        <div class="col-md-4">
          <button type="button" class="btn  btn-success " style="width:100%">delivered</button>
        </div>
        <div class="col-md-8">
          <p>PhaseHosting sent the email and it was accepted by the recipient email server.<br><br></p>
        </div>
        
      <!-- step 01 -->
        <div class="col-md-4">
          <button type="button" class="btn  btn-default " style="width:100%">accepted</button>
        </div>
        <div class="col-md-8">
          <p>PhaseHosting accepted the request to send/forward the email and the message has been placed in queue.<br><br></p>
        </div>

      <!-- step 01 -->
        <div class="col-md-4">
          <button type="button" class="btn  btn-warning " style="width:100%">complained</button>
        </div>
        <div class="col-md-8">
          <p>The email recipient clicked on the spam complaint button within their email client. Feedback loops enable the notification to be received by PhaseHosting.
<br><br></p>
        </div>

      <!-- step 01 -->
        <div class="col-md-4">
          <button type="button" class="btn  btn-danger " style="width:100%">rejected</button>
        </div>
        <div class="col-md-8">
          <p>PhaseHosting rejected the request to send/forward the email.<br><br></p>
        </div>

      <!-- step 01 -->
        <div class="col-md-4">
          <button type="button" class="btn  btn-danger " style="width:100%">failed</button>
        </div>
        <div class="col-md-8">
          <p>PhaseHosting could not deliver the email to the recipient email server.<br><br></p>
        </div>

      

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
