<!DOCTYPE html>
<link href="/templates/make/assets/global/plugins/maps-amcharts/ammap/ammap.min.css" rel="stylesheet">
<link href="/templates/make/assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">

<!-- BEGIN PAGE CONTENT -->
<div class="page-content" style="margin-top: 21px;">


	<div class="header">
   <h2><strong>Email</strong> Hosting</h2>
   <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      {foreach $breadcrumb as $item}
      <li{if $item@last} class="active"{/if}>
      {if !$item@last}<a href="{$item.link}">{/if}
      {$item.label}
      {if !$item@last}</a>{/if}
    </li>
    {/foreach}
  </ol>
</div>
</div>

<div class="col-lg-12 col-md-12">
  <div class="profil-content">
    <div class="col-md-8 col-md-offset-2">
      {if $errormessage}
        {include file="$template/includes/alert.tpl" type="error" errorshtml=$errormessage}
      {/if}
      <div class="panel">
        <div class="panel-header header-line">
          <h3>All Accounts</h3>
        </div>
        <div class="control-btn">
          <a data-toggle="modal" data-target="#help"><i class="icon-question"></i></a> 
        </div>
        <div class="panel-content">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Domain</th>
                <th>Package</th>
                <th>Status</th>
                <th>Next due date</th>
                <th>Options</th>
              </tr>
            </thead>
            <tbody>
            {foreach $emailHosting.products.product as $product}
              <tr>
              
                <td>{$product.id}</td>
                <td>{$product.domain}</td>
                <td>{$product.translated_name}</td>
                <td>{$product.status}</td>
                <td>{$product.nextduedate}</td>
                <td>
                  <a href="https://client.phasehosting.io/clientarea.php?action=productdetails&id={$product.id}&modop=custom&a=management&page=mailboxes" class="btn btn-primary btn-square">Configure</a>
                </td>

                

              </tr>
            {/foreach}
            </tbody>
          </table>
        </div>
        <div class="panel-footer clearfix">
          <div class="pull-right" data-toggle="modal" data-target="#addEmail">
            <button type="button" class="btn btn-sm btn-icon btn-rounded btn-primary" rel="popover" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Add a new domain for sending emails" data-original-title="New Email Domain" title=""><i class="fa fa-plus"></i></button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- modals -->
<div class="modal fade" id="addEmail" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>New</strong> Email</h4>
      </div>
      <div class="modal-body">

        <ul class="nav nav-tabs">
          <li class="active"><a href="#existingDomain" data-toggle="tab">Existing Domains</a></li>
          <li class=""><a href="#newDomain" data-toggle="tab">New Domain</a></li>

        </ul>


        <form method="post" action="email_order.php">
          <div class="tab-content">
            <div class="tab-pane fade active in" id="existingDomain">
              <div class="form-group">
                <label for="paymentmethod" class="control-label">Your Domains:</label><br/>
                <select name="existingDomain" id="paymentmethod" class="form-control">
                  {foreach $allProducts.products.product as $product}
                  {if $product.groupname != 'Email'}
                  {if $emailHosting.products.product.0.domain == {$product.domain} || $emailHosting.products.product.1.domain == {$product.domain} || $emailHosting.products.product.3.domain == {$product.domain} || $emailHosting.products.product.4.domain == {$product.domain} || $emailHosting.products.product.5.domain == {$product.domain} }

                  {else}
                  <option value="{$product.domain}"  
                  >{$product.domain}</option>
                  {/if}
                  {/if}
                  {/foreach}
                </select>
              </div>
            </div>
            <!-- Tab newDomain -->
            <div class="tab-pane" id="newDomain">
              <div class="form-group">
                <label for="paymentmethod" class="control-label">New Domain:</label><br/>
                <input name="newDomain" class="form-control form-white" disabled>
                <p>(Disabled during beta)</p>
              </div>
            </div>

            <!-- Package Select -->
            <div class="form-group">
                <label for="paymentmethod" class="control-label">Choose Plan:</label><br/>
                <select name="pid" id="paymentmethod" class="form-control">
                  
                  <option value="128">Free - 5 Mailboxes</option>
                  <option value="129">Professional - 15 Mailboxes - €15 p/m</option>
                  <option value="130">Business - 50 Mailboxes - €40 p/m </option>
                  
                </select>
              </div>

          </div>
        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary btn-embossed">Save changes</button>
        </div>
        </form>

      </div>
    </div>
  </div>

<!-- HELP -->
<div class="modal fade" id="help" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
				<h4 class="modal-title"><strong>Help</strong></h4>
			</div>
			<div class="modal-body">
			<!-- step 01 -->
				<div class="col-md-4">
					<h4><strong>01.</strong> Your Domain</h4>
				</div>
				<div class="col-md-8">
					<p>Click the &nbsp;<button type="button" class="btn btn-sm btn-icon btn-rounded btn-primary"><i class="fa fa-plus"></i></button> button to add your domain.</p>
					<div class="alert alert-success media fade in">
                      	<p><strong>Atention!</strong> During the beta stage we only accept domains that are linked to a hosting package.</p>
                    </div>
				</div>
			<!-- step 02 -->
				<div class="col-md-4">
					<h4><strong>02.</strong> Choose a Plan</h4>
				</div>
				<div class="col-md-8">
					<p>Choose your desired plan. 5 mailboxes are free to use and will remain free for ever. If more then 50 mailboxes are required please contact our support.</p>
				</div>
			<!-- step 03 -->
				<div class="col-md-4">
					<h4><strong>03.</strong> Save</h4>
				</div>
				<div class="col-md-8">
					<p>Click on save changes to activate your account.</p><br><br>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>