
<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
<script src="/templates/make2/assets/global/plugins/gsap/main-gsap.min.js"></script>
<script src="/templates/make2/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/templates/make2/assets/global/plugins/jquery-cookies/jquery.cookies.min.js"></script> <!-- Jquery Cookies, for theme -->
<script src="/templates/make2/assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script> <!-- simulate synchronous behavior when using AJAX -->
<script src="/templates/make2/assets/global/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
<script src="/templates/make2/assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script> <!-- Custom Scrollbar sidebar -->
<script src="/templates/make2/assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script> <!-- Show Dropdown on Mouseover -->
<script src="/templates/make2/assets/global/plugins/charts-sparkline/sparkline.min.js"></script> <!-- Charts Sparkline -->
<script src="/templates/make2/assets/global/plugins/retina/retina.min.js"></script> <!-- Retina Display -->
<script src="/templates/make2/assets/global/plugins/select2/select2.min.js"></script> <!-- Select Inputs -->
<script src="/templates/make2/assets/global/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs -->
<script src="/templates/make2/assets/global/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image -->
<script src="/templates/make2/assets/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script> <!-- Animated Progress Bar -->
<script src="/templates/make2/assets/global/js/builder.js"></script> <!-- Theme Builder -->
<script src="/templates/make2/assets/global/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
<script src="/templates/make2/assets/global/js/application.js"></script> <!-- Main Application Script -->
<script src="/templates/make2/assets/global/js/plugins.js"></script> <!-- Main Plugin Initialization Script -->
<script src="/templates/make2/assets/global/js/widgets/notes.js"></script> <!-- Notes Widget -->
<script src="/templates/make2/assets/global/js/quickview.js"></script> <!-- Chat Script -->
<script src="/templates/make2/assets/global/js/pages/search.js"></script> <!-- Search Script -->
<!-- BEGIN PAGE SCRIPT -->
<!--<script src="/templates/make2/assets/global/plugins/noty/jquery.noty.packaged.min.js"></script>  <!-- Notifications -->
<script src="/templates/make2/assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script> <!-- Inline Edition X-editable -->
<script src="/templates/make2/assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script> <!-- Context Menu -->
<script src="/templates/make2/assets/global/plugins/multidatepicker/multidatespicker.min.js"></script> <!-- Multi dates Picker -->
<script src="/templates/make2/assets/global/js/widgets/todo_list.js"></script>
<script src="/templates/make2/assets/global/plugins/metrojs/metrojs.min.js"></script> <!-- Flipping Panel -->
<script src="/templates/make2/assets/global/plugins/charts-chartjs/Chart.min.js"></script>  <!-- ChartJS Chart -->
<script src="/templates/make2/assets/global/plugins/charts-highstock/js/highstock.min.js"></script> <!-- financial Charts -->
<script src="/templates/make2/assets/global/plugins/charts-highstock/js/modules/exporting.min.js"></script> <!-- Financial Charts Export Tool -->
<script src="/templates/make2/assets/global/plugins/maps-amcharts/ammap/ammap.min.js"></script> <!-- Vector Map -->
<script src="/templates/make2/assets/global/plugins/maps-amcharts/ammap/maps/js/worldLow.min.js"></script> <!-- Vector World Map  -->
<script src="/templates/make2/assets/global/plugins/maps-amcharts/ammap/themes/black.min.js"></script> <!-- Vector Map Black Theme -->
<script src="/templates/make2/assets/global/plugins/skycons/skycons.min.js"></script> <!-- Animated Weather Icons -->
<script src="/templates/make2/assets/global/plugins/simple-weather/jquery.simpleWeather.js"></script> <!-- Weather Plugin -->
<script src="/templates/make2/assets/global/js/widgets/widget_weather.js"></script>
<script src="/templates/make2/assets/global/js/pages/dashboard.js"></script>
<!-- END PAGE SCRIPT -->
<!-- BEGIN PAGE SCRIPTS support-->
<script src="/templates/make2/assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script>
<script src="/templates/make2/assets/global/plugins/summernote/summernote.min.js"></script> <!-- Simple HTML Editor -->
<script src="/templates/make2/assets/global/plugins/quicksearch/quicksearch.min.js"></script> <!-- Search Filter -->
<script src="/templates/make2/assets/global/plugins/charts-morris/raphael.min.js"></script> <!-- Morris Charts -->
<script src="/templates/make2/assets/global/plugins/charts-morris/morris.min.js"></script> <!-- Morris Charts -->
<script src="/templates/make2/assets/global/js/pages/mailbox.js"></script>
<!-- END PAGE SCRIPTS -->
<script src="/templates/make2/assets/admin/layout2/js/layout.js"></script>
<script type="text/javascript">
var csrfToken = '{$token}';
</script>
<script src="{$WEB_ROOT}/templates/{$template}/js/whmcs.js"></script>
<script src="{$BASE_PATH_JS}/AjaxModal.js"></script>
{$footeroutput}

<script type="text/javascript">
var csrfToken = '{$token}',
markdownGuide = '{lang key="markdown.title"}',
locale = '{if !empty($mdeLocale)}{lang key="locale"}{else}en_GB{/if}',
saved = '{lang key="markdown.saved"}',
saving = '{lang key="markdown.saving"}';
</script>
<script src="{$WEB_ROOT}/templates/{$template}/js/whmcs.js"></script>
<script src="{$BASE_PATH_JS}/AjaxModal.js"></script>

<div class="modal system-modal fade" id="modalAjax" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content panel panel-primary">
      <div class="modal-header panel-heading">
        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times;</span>
          <span class="sr-only">Close</span>
        </button>
        <h4 class="modal-title">Title</h4>
      </div>
      <div class="modal-body panel-body">
        Loading...
      </div>
      <div class="modal-footer panel-footer">
        <div class="pull-left loader">
          <i class="fa fa-circle-o-notch fa-spin"></i> Loading...
        </div>
        <button type="button" class="btn btn-default" data-dismiss="modal">
          Close
        </button>
        <button type="button" class="btn btn-primary modal-submit">
          Submit
        </button>
      </div>
    </div>
  </div>
</div>
{literal}
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84743856-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='//rec.getsmartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', 'af18b585bc514733cb5695c52c2a6e4b38f5c719');
</script>
{/literal}
</body>
</html>
