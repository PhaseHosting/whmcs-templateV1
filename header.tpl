<!DOCTYPE html>
{if $loginpage ne "true"}
{if ($filename!='register')}
{if ($templatefile!='homepage')}
{if ($filename!='pwreset')}
{if ($templatefile!='logintwofa')}
{if ($templatefile!='logout')}
{if ($filename!='pwreset')}
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="PhaseHosting Client Area">
	<meta name="author" content="Ilyas Deckers">
	<link rel="shortcut icon" href="/templates/make/assets/global/images/favicon.png" type="image/png">
	<title>{if $kbarticle.title}{$kbarticle.title} - {/if}{$pagetitle} - {$companyname}</title>
	<link href="/templates/make2/assets/global/css/style.css" rel="stylesheet">
	<link href="/templates/make2/assets/global/css/theme.css" rel="stylesheet">
	<link href="/templates/make2/assets/global/css/ui.css" rel="stylesheet">
	<link href="/templates/make2/assets/global/css/meter.css" rel="stylesheet">
	<link href="/templates/make2/assets/admin/layout2/css/layout.css" rel="stylesheet">
	<link href="/templates/make2/font-mfizz.css" rel="stylesheet">
	<!-- BEGIN PAGE STYLE -->
	<link href="/templates/make2/assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">
	<link href="/templates/make2/assets/global/plugins/maps-amcharts/ammap/ammap.min.css" rel="stylesheet">
	<!-- END PAGE STYLE -->
	<script src="/templates/make2/assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	
	<script src="/templates/make2/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
	<script src="/templates/make2/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
	<script src="/templates/make2/assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
	
</head>

<body class="sidebar-condensed fixed-sidebar fixed-topbar theme-sdtl bg-clean color-default {if $pagetitle == "Builder"}sidebar-collapsed{/if}">
	<!-- BEGIN PRELOADER -->
	<div class="loader-overlay">
		<div class="spinner">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div>
	<!-- END PRELOADER -->
  <!--[if lt IE 7]>
  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->
  <section>
  	<!-- BEGIN SIDEBAR -->
  	<div class="sidebar">
  		<div class="logopanel" >
  			<h1>
  				<a href="/clientarea.php" style="color:white;text-transform: uppercase;" style=""><center>phase</center></a>
  			</h1>
  		</div>
  		<div class="sidebar-inner">
  			<div class="menu-title">
  				Navigation 
          <!-- <div class="pull-right menu-settings">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300" aria-expanded="false"> 
              <i class="icon-settings"></i>
            </a>
            <ul class="dropdown-menu">
              <li><a href="#" id="reorder-menu" class="reorder-menu">Reorder menu</a></li>
              <
            </ul>
        </div> -->
    </div>
    <ul class="nav nav-sidebar">
    	<li class="nav			{if $templatefile eq "clientareahome"}active{/if}"><a href="/clientarea.php"><i class="icon-home"></i><span>Dashboard</span></a></li>
    	<li class="nav 
    	{if $templatefile eq "clientareadomains"}active{/if}
    	{if $templatefile eq "domainchecker"}active{/if}">
    	<a href="/clientarea.php?action=domains"><i class="fa fa-globe"></i><span>{$LANG.navdomains}</span></a>
    	
    </li>
    
         <!-- <li class="nav-parent {if $templatefile eq "webhosting"}active{/if}">
            <a href="/webhosting.php"><i class="fa fa-desktop"></i><span>Websites</span><span class="fa arrow"></a>
              <ul class="children collapse">
                <li><a href="/cart.php?carttpl=make"> Web Hosting</a></li>
                <li><a href="/cart.php?gid=4?carttpl=make"> Reseller Hosting</a></li>
              </ul>
          </li>-->
          <li class="nav {if $templatefile eq "webhostting"}active{/if}">
          	<a href="/webhosting.php"><i class="fa fa-desktop"></i><span>Websites</span></a>
          </li>
            <!-- <li class="nav {if $templatefile eq "wordpresshosting"}active{/if}">
              <a href="/wordpresshosting.php"><i class="fa fa-wordpress"></i><span>WordPress</span></a>
          </li> -->
          
          
            <!-- <li class="nav-active">
            <a href="#"><i class="fa fa-wordpress"></i><span>WordPress</span> <span class="fa arrow"></span></a>
        </li> -->
        
        <li class="nav {if $templatefile eq "email"}active{/if}">
        	<a href="email_accounts.php"><i class="fa fa-envelope"></i><span>Email</span></a>
        </li>
        <!-- <li class="nav {if $templatefile eq "vpshosting"}active{/if}">
        	<a href="/vpshosting.php"><i class="fa fa-server"></i><span>Servers</span></a>
        </li> -->
        <li class="nav {if $pagetitle eq "Builder"}active{/if}">
        	<a href="/builder.php"><i class="fa fa-check-square-o"></i><span>Builder</span></a>
        </li>
        <li class="nav-parent 
        {if $templatefile eq "clientareaquotes"}active{/if}
        {if $templatefile eq "clientareainvoices"}active{/if}
        {if $templatefile eq "clientareaaddfunds"}active{/if}
        {if $templatefile eq "masspay"}active{/if}
        {if $templatefile eq "affiliates"}active{/if}
        ">
        <a href="/cart.php?carttpl=make"><i class="fa  fa-money"></i><span>{$LANG.navbilling}</span> <span class="fa arrow"></span></a>
        <ul class="children collapse">
        	<li><a href="/clientarea.php?action=invoices"> {$LANG.invoices}</a></li>
        	<li><a href="/clientarea.php?action=quotes"> {$LANG.quotestitle}</a></li>
        	<li><a href="/clientarea.php?action=masspay&all=true"> {$LANG.masspaytitle}</a></li>
        	<li><a href="/affiliates.php"> {$LANG.affiliatestitle  }</a></li>
        	<li><a href="/clientarea.php?action=addfunds"> {$LANG.addfunds}</a></li>
        </ul>
    </li>
    <li class="nav-parent 
    {if $templatefile eq "supportticketslist"}active{/if}
    {if $templatefile eq "viewticket"}active{/if}
    {if $templatefile eq "supportticketsubmit-stepone"}active{/if}
    {if $templatefile eq "supportticketsubmit-steptwo"}active{/if}
    {if $templatefile eq "knowhow"}active{/if}
    ">
    <a href=""><i class="fa fa-question"></i><span>{$LANG.navsupport}</span><span class="fa arrow"></span></a>
    <ul class="children collapse">
    	<li><a href="/supporttickets.php"> {$LANG.navtickets}</a></li>
    	<li><a href="/submitticket.php?step=2&deptid=1"> {$LANG.navopenticket}</a></li>
    	<li><a href="https://phasehosting.be/docs" target="_blank"> {$LANG.knowledgebasetitle}</a></li>
    </ul>
</li>
</ul>
  <!-- SIDEBAR WIDGET FOLDERS 
  <div class="sidebar-widgets">
  <p class="menu-title widget-title">Statistics <span class="pull-right"><a href="#" class="hide-widget"> <i class="icons-office-52"></i></a></span></p>
  <div class="charts-sidebar progress-chart">
  <div class="sidebar-charts-inner">
  <div class="clearfix">
  <div class="sidebar-chart-title">Stat 1</div>
  <div class="sidebar-chart-number">82%</div>
</div>
<div class="progress">
<div class="progress-bar progress-bar-primary stat1" data-transitiongoal="82"></div>
</div>
</div>
<div class="sidebar-charts-inner">
<div class="clearfix">
<div class="sidebar-chart-title">Stat 2</div>
<div class="sidebar-chart-number">43%</div>
</div>
<div class="progress">
<div class="progress-bar progress-bar-primary stat2" data-transitiongoal="43"></div>
</div>
</div>
<div class="sidebar-charts-inner">
<div class="clearfix">
<div class="sidebar-chart-title">Stat 3</div>
<div class="sidebar-chart-number" id="number-visits">93%</div>
</div>
<div class="progress">
<div class="progress-bar progress-bar-primary stat3" data-transitiongoal="93"></div>
</div>
</div>
</div>
</div>-->
<div class="sidebar-footer clearfix">
	<a class="pull-left footer-settings" href="/clientarea.php?action=details" data-rel="tooltip" data-placement="top" data-original-title="Settings">
		<i class="icon-settings"></i></a>
		<a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top" data-original-title="Fullscreen">
			<i class="icon-size-fullscreen"></i></a>
			<a class="pull-left btn-effect" href="/logout.php" data-modal="modal-1" data-rel="tooltip" data-placement="top" data-original-title="Logout">
				<i class="icon-power"></i></a>
			</div>
		</div>
	</div>
	<div class="main-content">
		<div class="topbar">
			<div class="header-left">
				<div class="topnav">
					<a class="menutoggle"  data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>
					<ul class="nav nav-icons">
						<li><a href="/clientarea.php?action=details"><span class="icon-user-following"></span></a></li>
					</ul>
				</div>
			</div>
			<div class="header-right">
				<ul class="header-menu nav navbar-nav">
					{if ($cartitemcount!='0')}
					<li class="dropdown" id="notifications-header">
						<a href="/cart.php?a=view" >
							<i class="fa fa-shopping-cart"></i>
							
							<span class="badge badge-danger badge-header">{$cartitemcount}</span>
							
						</a>
					</li>
					{/if}
					<li class="dropdown" id="language-header">
						<a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
							<i class="icon-globe"></i>
							<span>Language</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="{$currentpagelinkback}language=english" data-lang="en"><img src="/templates/make2/assets/global/images/flags/United-Kingdom.png" alt="flag-english"> <span>English</span></a>
							</li>
							<li>
								<a href="{$currentpagelinkback}language=dutch" data-lang="es"><img src="/templates/make2/assets/global/images/flags/Netherlands.png" alt="flag-english"> <span>Nederlands</span></a>
							</li>
							<li>
								<a href="{$currentpagelinkback}language=french" data-lang="fr"><img src="/templates/make2/assets/global/images/flags/french.png" alt="flag-english"> <span>Français</span></a>
							</li>
						</ul>
					</li>
					{if $loggedin}
					<li class="dropdown" id="user-header">
						<a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<img src="{gravatar email=$clientsdetails.email rating="PG" size="60" }" alt="user image">
							<span class="username">Hi, {$clientsdetails.firstname}</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="/clientarea.php"><i class="icon-user"></i><span>My Profile</span></a>
							</li>
							<li>
								<a href="/clientarea.php?action=details"><i class="icon-settings"></i><span>Account Settings</span></a>
							</li>
							<li>
								<a href="/clientarea.php?action=changepw"><i class="fa fa-key"></i><span>Change Password</span></a>
							</li>
							<li>
								<a href="/logout.php"><i class="icon-logout"></i><span>Logout</span></a>
							</li>
						</ul>
					</li>
					{/if}
					{if $loggedin}
					<li class="dropdown" id="notifications-header">
						<a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<i class="icon-bell"></i>
							<span class="badge badge-danger badge-header">{$clientAlerts|count}</span>
						</a>
						<ul class="dropdown-menu">
							<li class="dropdown-header clearfix">
								<p class="pull-left">{$clientAlerts|count} {$LANG.notifications} </p>
							</li>
							<li>
								<ul class="dropdown-menu-list withScroll" data-height="220">
									{foreach $clientAlerts as $alert}
									<li>
										<a href="{$alert->getLink()}">
											<i class="fa fa-star p-r-10 f-18 c-orange"></i>
											{$alert->getMessage()}{if $alert->getLinkText()} <a href="{$alert->getLink()}" class="btn btn-xs btn-{$alert->getSeverity()}">{$alert->getLinkText()}</a>{/if}
											
										</a>
									</li>
									{foreachelse}
									<li>
										<a href="#">
											<i class="fa fa-heart p-r-10 f-18 c-red"></i>
											{$LANG.notificationsnone}
											
										</a>
									</li>
									{/foreach}
								</ul>
							</li>
							<li class="dropdown-footer clearfix">
								<a href="#" class="pull-right">
									<i class="icon-settings"></i>
								</a>
							</li>
						</ul>
					</li>
				</li>
				{/if}
				<!-- END NOTIFICATION DROPDOWN -->
				<!-- <li id="quickview-toggle"><a href="#"><i class="icon-bubbles"></i></a></li> -->
				


			</ul>
		</div>
		<!-- header-right -->
	</div>
	<!-- END TOPBAR -->
	{/if}
	{/if}
	{/if}
	{/if}
	{/if}
	{/if}
	{/if}
