<!DOCTYPE html>
<div class="row profil-header">
  <div class="col-lg-9 col-md-12" >
    <div class="row">
      <div class="col-xs-12 p-l-0 col-map">
        <div class="map"><img src="/templates/make2/assets/global/images/profil_page/header.png" width="100%"></img></div>
      </div>
    </div>
    <div class="row header-name">
      <div class="col-xs-9">
        <div class="name">{$clientsdetails.fullname}
          {if $emailVerificationIdValid}
          <i class="fa fa-check-circle" style="color:#39B77B;"></i>
          {elseif $emailVerificationIdValid === false}
          <i class="fa fa-check-circle" style="color:#39B77B;"></i>
          {elseif $emailVerificationPending && !$showingLoginPage}
          <i class="fa fa-warning " rel="popover" style="color:#F30909;" data-container="body" data-toggle="popover" data-placement="top" data-content="{$LANG.verifyEmailAddress}" data-original-title="" ></i>
          <button id="btnResendVerificationEmail" class="btn btn-default btn-sm">
            {$LANG.resendEmail}
          </button>
		  {else}
		  <i class="fa fa-check-circle" style="color:#39B77B;" rel="popover" data-container="body" data-toggle="popover" data-placement="top" data-content="Email verified" data-original-title=""></i>
          {/if}
          
          
        </div>
        <div class="profil-info"><i class="fa  fa-money"></i>{$clientsstats.creditbalance}</div>
        <div class="profil-info"><i class="fa fa-envelope-o"></i>{$clientsdetails.email}</div>
      </div>
      {if $invoices	}
      <a href="/clientarea.php?action=invoices">
        <div class="col-sm-3 moments hidden-xs" style="background: #E9ECED;">
          
          <div class="num-moments c-primary countup" data-from="0" data-to="137" data-delay="2000" data-suffix="+" data-duration="6">{$clientsstats.numunpaidinvoices}</div>
          <div class="text-center">Invoices</div>
          
        </div>
      </a>
      {else}
      <div class="col-xs-3 bg-white">
      </div>
      {/if}
    </div>
  </div>
  <div class="col-lg-3 col-md-12 user-friends">
    <div class="row m-r-0">
      <div class="col-sm-4 bg-red more-friends">
        {if $loggedin}
        <span class="num-friends">{$clientsstats.numactivetickets}</span>
        <p>{$LANG.navtickets}</p>
        {/if}
      </div>
      
      
      
      <div class="col-sm-4 bg-yellow">
        <span class="num-friends"> &nbsp; </span>
        <p> &nbsp;</p>
      </div>
      <div class="col-sm-4">
        <img src="/templates/make2/assets/global/images/profil_page/friend7.jpg" class="img-responsive" alt="friend 8">
      </div>
    </div>
    <div class="row m-r-0">
      
      <div class="col-sm-4">
        <img src="/templates/make2/assets/global/images/profil_page/friend6.jpg" class="img-responsive" alt="friend 6">
      </div>
      <div class="col-sm-4 bg-orange more-friends">
        {if $loggedin}
        <span class="num-friends"> {$clientsstats.productsnumactive} </span>
        <p>{$LANG.navservices}</p>
        {/if}
      </div>
      <div class="col-sm-4 bg-blue more-friends">
        <span class="num-friends"> &nbsp; </span>
        <p> &nbsp;</p>
      </div>
    </div>
    <div class="row m-r-0">
      <div class="col-sm-4 bg-primary more-friends">
        {if $loggedin}
        {if $registerdomainenabled || $transferdomainenabled}
        <span class="num-friends">{$clientsstats.numactivedomains}</span>
        <p> {$LANG.navdomains} </p>
        {else}
        <span class="num-friends"> &nbsp; </span>
        <p> &nbsp;</p>
        {/if}
        {/if}
      </div>
      
      <div class="col-sm-4 bg-green">
        
      </div>
      
      <div class="col-sm-4 bg-primary more-friends">
        
      </div>
    </div>
  </div>
</div>
{include file="$template/includes/verifyemail.tpl"}