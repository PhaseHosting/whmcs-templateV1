<div>
&nbsp;&nbsp;&nbsp;
</div>
{if $emailVerificationIdValid}
    <div class="alert-success">
        <div class="container">
            <i class="fa fa-check"></i>
            {$LANG.emailAddressVerified}
        </div>
    </div>
{elseif $emailVerificationIdValid === false}
    <div class="alert-danger">
        <div class="container">
            <i class="fa fa-times-circle"></i>
            {$LANG.emailKeyExpired}
            <div class="pull-right">
                <button id="btnResendVerificationEmail" class="btn btn-default btn-sm">
                    {$LANG.resendEmail}
                </button>
            </div>
        </div>
    </div>
{elseif $emailVerificationPending && !$showingLoginPage}

{/if}
