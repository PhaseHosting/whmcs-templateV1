<div class="page-content page-app page-profil" style="margin-top: 21px;">
  <div class="col-lg-10 col-md-9">
    {include file="$template/header_client.tpl"}
    <div class="profil-content">
      <div class="col-md-12>
        <form role="form" method="post" action="{$WEB_ROOT}/knowledgebase.php?action=search">
          <div class="input-group">
            <input type="text" name="search" class="form-control" placeholder="{$LANG.kbsearchexplain}" />
            <span class="input-group-btn">
              <input type="submit" class="btn btn-primary btn-input-padded-responsive" value="{$LANG.search}" />
            </span>
          </div>
        </form>
      </div>
      <div class="col-md-4">
        <div class="panel">
          <div class="panel-content">
            <h1>{$LANG.knowledgebasecategories}</h1>

            {if $kbcats}


            {foreach from=$kbcats item=kbcat}
            <div class="col-sm-4 text-center">
              
              <a href="{if $seofriendlyurls}{$WEB_ROOT}/knowledgebase/{$kbcat.id}/{$kbcat.urlfriendlyname}{else}knowledgebase.php?action=displaycat&amp;catid={$kbcat.id}{/if}">
                <span class="badge badge-info">{$kbcat.numarticles}</span>
				<h3 class="text-center">{$kbcat.name} </h3>
              </a>

            </div>
            {/foreach}

            <p>&nbsp;</p>
          </div>
        </div>
        {else}
        {include file="$template/includes/alert.tpl" type="info" msg=$LANG.knowledgebasenoarticles textcenter=true}
        {/if}
      </div>
      <div class="col-md-8">
        {if $kbmostviews}
        <div class="panel">
          <div class="panel-content">
            <h2>{$LANG.knowledgebasepopular}</h2>

            <div class="kbarticles">
              {foreach from=$kbmostviews item=kbarticle}
              <a href="{if $seofriendlyurls}{$WEB_ROOT}/knowledgebase/{$kbarticle.id}/{$kbarticle.urlfriendlytitle}.html{else}knowledgebase.php?action=displayarticle&amp;id={$kbarticle.id}{/if}">
                <span class="glyphicon glyphicon-file"></span>&nbsp;{$kbarticle.title}
              </a>
              <p>{$kbarticle.article|truncate:100:"..."}</p>
              {/foreach}
            </div>
          </div>
        </div>
        {/if}
      </div>
    </div>
  </div>
  {include file="$template/sidebar_client.tpl"}
</div>
