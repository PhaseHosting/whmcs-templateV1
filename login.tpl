<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Login - {$companyname}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta content="" name="description" />
  <meta content="Ilyas Deckers" name="author" />
  <link rel="shortcut icon" href="/templates/make/assets/global/images/favicon.png">
  <link href="/templates/make/assets/global/css/style.css" rel="stylesheet">
  <link href="/templates/make/assets/global/css/ui.css" rel="stylesheet">
  <link href="/templates/make/assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
  {literal}
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
    ga('create', 'UA-72170997-4', 'auto');
    ga('send', 'pageview');
    
  </script>
  {/literal}
</head>


<body class="sidebar-condensed account2" data-page="login">
  
  <!-- BEGIN LOGIN BOX -->
  <div class="container" id="login-block">
    <i class="user-img icons-faces-users-03"></i>
    <div class="account-info">
      <h1>Phase<br><strong>hosting</strong></h1>
      <!-- <h3><strong>Control</strong> Panel</h3> -->
      <ul>
        <li><i class="icon-layers"></i> Web Hosting</li>
        <li><i class="icon-arrow-right"></i> VPS Hosting</li>
        <li><i class="icon-drop"></i> Administration</li>
        <li><i class="icon-magic-wand"></i> Website Builder</li>

      </ul>
    </div>
    <div class="account-form">
      <form class="form-signin" role="form" action="{$systemsslurl}dologin.php" method="post">
        <h3><strong>Sign in</strong> to your account</h3>
        <div class="append-icon">
          <input type="email" name="username" id="name" class="form-control form-white username" placeholder="{$LANG.clientareaemail}" required>
          <i class="icon-user"></i>
        </div>
        <div class="append-icon m-b-20">
          <input type="password" name="password" class="form-control form-white password" placeholder="{$LANG.clientareapassword}" required>
          <i class="icon-lock"></i>
        </div>
        <button type="submit" class="btn btn-lg btn-dark btn-rounded ladda-button" data-style="expand-left">{$LANG.loginbutton}</button>
        <span class="forgot-password"><a href="/pwreset.php">Forgot password?</a></span>
        {if $incorrect}
        {include file="$template/includes/alert.tpl" type="error" msg=$LANG.loginincorrect textcenter=true}
        {elseif $ssoredirect}
        {include file="$template/includes/alert.tpl" type="info" msg=$LANG.sso.redirectafterlogin textcenter=true}
        {/if}
        
        <div class="form-footer">
          <div class="clearfix">
            <p class="new-here"><a href="/register.php">New here? Sign up</a></p>
          </div>
        </div>
      </form>
      
      
    </div>
    <!-- END LOCKSCREEN BOX -->
    <script src="/templates/make/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
    <script src="/templates/make/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
    <script src="/templates/make/assets/global/plugins/gsap/main-gsap.min.js"></script>
    <script src="/templates/make/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/templates/make/assets/global/plugins/backstretch/backstretch.min.js"></script>
    <script src="/templates/make/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <!--<script src="/templates/make/assets/global/js/pages/login-v2.js"></script>
    <script src="/templates/make/assets/admin/layout2/js/layout.js"></script>-->

  </body>
  </html>
