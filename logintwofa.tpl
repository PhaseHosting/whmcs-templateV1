<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{if $kbarticle.title}{$kbarticle.title} - {/if}{$pagetitle} - {$companyname}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="Ilyas Deckers" name="author" />
        <link rel="shortcut icon" href="/templates/make/assets/global/images/favicon.png">
        <link href="/templates/make/assets/global/css/style.css" rel="stylesheet">
        <link href="/templates/make/assets/global/css/ui.css" rel="stylesheet">
        <link href="/templates/make/assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
    </head>

    <body class="sidebar-condensed account2" data-page="login">
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <i class="user-img icons-faces-users-03"></i>
            <div class="account-info">
                <h1>Phase</h1>
                <h3>Control & Administration.</h3>
                <ul>
                    <li><i class="icon-magic-wand"></i> Your Cloud</li>
                    <li><i class="icon-layers"></i> Web Hosting</li>
                    <li><i class="icon-arrow-right"></i> VPS Hosting</li>
                    <li><i class="icon-drop"></i> Administration</li>
                </ul>
            </div>
            <div class="account-form">
                <form class="form-signin" role="form" action="{$systemsslurl}dologin.php" id="frmLogin" method="post">
				<input type="hidden" name="action" value="reset" />
                    <h3><strong>Two</strong> factor authentication</h3>
					{if $newbackupcode}
        {include file="$template/includes/alert.tpl" type="success" msg=$LANG.twofabackupcodereset textcenter=true}
    {elseif $incorrect}
        {include file="$template/includes/alert.tpl" type="error" msg=$LANG.twofa2ndfactorincorrect textcenter=true}
    {elseif $error}
        {include file="$template/includes/alert.tpl" type="error" msg=$error textcenter=true}
    {else}
        {include file="$template/includes/alert.tpl" type="warning" msg=$LANG.twofa2ndfactorreq textcenter=true}
    {/if}
                    {if $newbackupcode}
            <input type="hidden" name="newbackupcode" value="1" />
            <h2 class="text-center">{$LANG.twofanewbackupcodeis}</h2>
            
            <p class="text-center">{$LANG.twofabackupcodeexpl}</p>
            <p class="text-center"><input type="submit" value="{$LANG.continue} &raquo;" class="btn" /></p>
        {elseif $backupcode}
            <br/>
            <input id="login" type="submit" class="btn btn-primary btn-block" value="{$LANG.loginbutton}" />
            <input type="hidden" name="backupcode" value="1" />
        {else}
            <div class="margin-bottom">
                {$challenge}
            </div>
            
		<div class="form-footer">
          <div class="clearfix">
            <p class="new-here"><a href="clientarea.php?backupcode=1">{$LANG.twofaloginusingbackupcode}</a></p>
          </div>
        </div>
            </div>
        {/if}
		
				</form>
                
            </div>
           
        <!-- END LOCKSCREEN BOX -->
        <script src="/templates/make/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="/templates/make/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="/templates/make/assets/global/plugins/gsap/main-gsap.min.js"></script>
        <script src="/templates/make/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/templates/make/assets/global/plugins/backstretch/backstretch.min.js"></script>
        <script src="/templates/make/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <!--<script src="/templates/make/assets/global/js/pages/login-v2.js"></script>
      <script src="/templates/make/assets/admin/layout2/js/layout.js"></script>-->
  </body>
</html>
