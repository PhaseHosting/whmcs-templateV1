
<div id="delete" title="Delete OAuth Key" style="display:none;">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 40px 0;"></span>Are you sure you want to delete this key? This will permanently disable this key.</p>
</div>

{literal}
<script type="text/javascript" src="/includes/jscript/jqueryui.js"></script> 
<link href="/includes/jscript/css/ui.all.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript">

function showDialog(name, url) {
	jQuery("#delete").dialog({
		autoOpen: false,
		resizable: false,
		height: 180,
		modal: true,
		buttons: {'Yes': function() {
				window.location=url;
			},'No': function() {
				$(this).dialog('close');
			}}
	});
	jQuery("#"+name).dialog('open');
}

</script>
{/literal}

{if $addedkey == "Invalid Information"}
<div class="errorbox" style="margin-bottom: 3px; margin-top: 3px;">
An error occured while creating your API key.
</div>
{elseif $addedkey}
<div class="successbox" style="margin-bottom: 3px; margin-top: 3px;">
Your new API key has been created.<br /><br />
Consumer Key: {$addedkey.consumerkey}<br />
Secret Key: {$addedkey.secret}
</div>
{/if}

<table class="form" cellspacing="2" cellpadding="3" border="0" width="100%" style="border-collapse: collapse;">
	<tr>
    	<td style="font-weight:bold;border: 1px solid #75B8DC; background-color: #CBE1F7;">Name</td>
        <td style="font-weight:bold;border: 1px solid #75B8DC; background-color: #CBE1F7;">Consumer Key</td>
        <td style="font-weight:bold;border: 1px solid #75B8DC; background-color: #CBE1F7;">Secret Key</td>
        <td style="font-weight:bold;border: 1px solid #75B8DC; background-color: #CBE1F7;">Action</td>
    </tr>
    
    {if $iskeys}
    	{foreach from=$oauthkeys key=k item=oKey}
	<tr>
    	<td style="border: 1px solid #75B8DC;">{$oKey.name}</td>
        <td style="border: 1px solid #75B8DC;">{$oKey.appid}</td>
        <td style="border: 1px solid #75B8DC;">{$oKey.appsecret}</td>
        <td style="border: 1px solid #75B8DC;">
        	<img src="images/statusfailed.gif" title="Delete Key" alt="Delete Key" border="0" onclick="showDialog('delete', 'oauth.php?deleteKey={$oKey.id}');" />
        </td>
    </tr>
    	{/foreach}    
    {else}
    <tr>
    	<td style="border: 1px solid #75B8DC;" colspan="4">No API keys available.</td>
    </tr>
    {/if}
</table>


<div style="margin: 5px;">
<form method="post" action="oauth.php">
    <input type="hidden" name="action" value="createkey" />
    <strong style="margin-bottom: 3px; border-bottom: 1px solid;">Add API Key</strong>
    <div style="padding: 5px;">
    	Name: <input type="text" name="name" /> <input type="submit" value="Add Key" />
    </div>
</form>
</div>