<!DOCTYPE html>
<div class="page-content" style="margin-top: 21px;">


	<div class="header">
		<h2><strong>VPS</strong> Instances</h2>

		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				<!-- {foreach $breadcrumb as $item}
				<li{if $item@last} class="active"{/if}>
				{if !$item@last}<a href="{$item.link}">{/if}
				{$item.label}
				{if !$item@last}</a>{/if}
			</li>
			{/foreach} -->
		</ol>
	</div>
</div>

<div class="col-lg-7 col-md-7">
	<div class="profil-content">
		<div class="col-md-2">

		</div>
		<div class="col-md-8">
			<div class="col-md-6">
				<a href="/cart.php?gid=4">
					<div class="panel">
						<div class="panel-content text-center">
							<i class="fa fa-4x fa-server"></i>
							<h2><strong>Compute</strong> Instances</h2>
						</div>
					</div>
				</a>
			</div>

			<div class="col-md-6">
				<a href="/cart.php?gid=16">
					<div class="panel">
						<div class="panel-content text-center">
							<i class="fa fa-4x fa-th-large"></i>
							<h2><strong>Windows</strong> Instances</h2>
						</div>
					</div>
				</a>
			</div>

			<div class="col-md-6">
				<a href="/cart.php?gid=19">
					<div class="panel">
						<div class="panel-content text-center">
							<i class="fa fa-4x fa-database"></i>
							<h2><strong>Storage</strong> Instances</h2>
						</div>
					</div>
				</a>
			</div>


			<!-- <div class="col-md-6">
				<div class="panel">
					<div class="panel-content text-center">
						<i class="fa fa-4x fa-magic"></i>
						<h2><strong>One-</strong>click</h2><p></p>
					</div>
				</div>
			</div> -->

			<div class="col-md-6">
				<div class="panel">
					<div class="panel-content text-center">
						<i class="fa fa-4x fa-server"></i>
						<h2><strong>Dedicated</strong> Servers</h2><p class="text-center">(Contact Support)</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-2">

		</div>

	</div>
</div>
<div class="col-md-5">
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-content">
				<h1>Distributions <strong>&</strong> <br>One-Click Install Apps</h1>
				<p>Choose your favorite Linux distribution or install your favorite application with the simple click of a button. Click on the Application and be live on a cloud server under 15 minutes.
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="panel">
			<div class="panel-content">

				<h1><strong>Multiple</strong> Datacenter Locations</h1>
				<p>Select the best location based on latency, or deploy across regions for high availability. You can choose your desired datacenter from one of our 14 locations</p>


				<img src="https://phasehosting.io/wp-content/uploads/2016/03/world.png">

			</div>
		</div>
	</div>
</div>