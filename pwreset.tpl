<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login - {$companyname}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="Ilyas Deckers" name="author" />
        <link rel="shortcut icon" href="/templates/make/assets/global/images/favicon.png">
        <link href="/templates/make/assets/global/css/style.css" rel="stylesheet">
        <link href="/templates/make/assets/global/css/ui.css" rel="stylesheet">
        <link href="/templates/make/assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
    </head>
	

    <body class="sidebar-condensed account2" data-page="login">
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <i class="user-img icons-faces-users-03"></i>
            <div class="account-info">
                <h1>Phase</h1>
                <h3>Control & Administration.</h3>
                <ul>
                    <li><i class="icon-magic-wand"></i> Your Cloud</li>
                    <li><i class="icon-layers"></i> Web Hosting</li>
                    <li><i class="icon-arrow-right"></i> VPS Hosting</li>
                    <li><i class="icon-drop"></i> Administration</li>
                </ul>
            </div>
            <div class="account-form">
                <form class="form-signin" role="form" action="{$systemsslurl}pwreset.php" method="post">
				<input type="hidden" name="action" value="reset" />
                    <h3><strong>Reset</strong> your password</h3>
                    <div class="append-icon m-b-20">
                        <input type="email" name="email" class="form-control form-white" placeholder="{$LANG.enteremail}" required>
                        <i class="icon-lock"></i>
                    </div>
                    <button type="submit" class="btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left">{$LANG.pwresetsubmit}</button>
					{if $success}

            {include file="$template/includes/alert.tpl" type="success" msg=$LANG.pwresetvalidationsent textcenter=true}

            <p class="text-center">{$LANG.pwresetvalidationcheckemail}</p>
			{/if}
                    <div class="clearfix m-t-60">
                        <p class="pull-left m-t-20 m-b-0"><a  href="login.php">Have an account? Sign In</a></p>
                        <p class="pull-right m-t-20 m-b-0"><a href="register.php">New here? Sign up</a></p>
                    </div>
				</form>
            </div>
           
        <!-- END LOCKSCREEN BOX -->
        <script src="/templates/make/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="/templates/make/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="/templates/make/assets/global/plugins/gsap/main-gsap.min.js"></script>
        <script src="/templates/make/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/templates/make/assets/global/plugins/backstretch/backstretch.min.js"></script>
        <script src="/templates/make/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <!--<script src="/templates/make/assets/global/js/pages/login-v2.js"></script>
      <script src="/templates/make/assets/admin/layout2/js/layout.js"></script>-->
  </body>
</html>