<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login - {$companyname}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="Ilyas Deckers" name="author" />
        <link rel="shortcut icon" href="/templates/make/assets/global/images/favicon.png">
        <link href="/templates/make/assets/global/css/style.css" rel="stylesheet">
        <link href="/templates/make/assets/global/css/ui.css" rel="stylesheet">
        <link href="/templates/make/assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
    </head>
	
	
    <body class="sidebar-condensed account2" data-page="login">
	
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <i class="user-img icons-faces-users-03"></i>
            <div class="account-info">
                <h1>Phase</h1>
                <h3>Control & Administration.</h3>
                <ul>
                    <li><i class="icon-magic-wand"></i> Your Cloud</li>
                    <li><i class="icon-layers"></i> Web Hosting</li>
                    <li><i class="icon-arrow-right"></i> VPS Hosting</li>
                    <li><i class="icon-drop"></i> Administration</li>
                </ul>
            </div>
            <div class="account-form">
    {if $invalidlink}

        {include file="$template/includes/alert.tpl" type="danger" msg=$invalidlink textcenter=true}

    {elseif $success}

        {include file="$template/includes/alert.tpl" type="success" msg=$LANG.pwresetvalidationsuccess textcenter=true}

        <p class="text-center">
            {$LANG.pwresetsuccessdesc|sprintf2:'<a href="clientarea.php">':'</a>'}
        </p>

    {else}

        {if $errormessage}
            {include file="$template/includes/alert.tpl" type="danger" msg=$errormessage textcenter=true}
        {/if}

        <p>{$LANG.pwresetenternewpw}</p>

        <form class="using-password-strength" method="post" action="{$smarty.server.PHP_SELF}?action=pwreset">
            <input type="hidden" name="key" id="key" value="{$key}" />

            <div id="newPassword1" class="form-group has-feedback">
                <label class="control-label" for="inputNewPassword1">{$LANG.newpassword}</label>
                <input type="password" name="newpw" id="inputNewPassword1" class="form-control" />
                <span class="form-control-feedback glyphicon glyphicon-password"></span>
            </div>

            <div id="newPassword2" class="form-group has-feedback">
                <label class="control-label" for="inputNewPassword2">{$LANG.confirmnewpassword}</label>
                <input type="password" name="confirmpw" id="inputNewPassword2" class="form-control" />
                <span class="form-control-feedback glyphicon glyphicon-password"></span>
                <div id="inputNewPassword2Msg"></div>
            </div>

            <div class="form-group">
                <label class="control-label">{$LANG.pwstrength}</label>
                {include file="$template/includes/pwstrength.tpl"}
            </div>

            <div class="form-group">
                <div class="text-center">
                    <input class="btn btn-primary" type="submit" name="submit" value="{$LANG.clientareasavechanges}" />
                    <input class="btn btn-default" type="reset" value="{$LANG.cancel}" />
                </div>
            </div>

        </form>

    {/if}
            </div>
           
        <!-- END LOCKSCREEN BOX -->
        <script src="/templates/make/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="/templates/make/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="/templates/make/assets/global/plugins/gsap/main-gsap.min.js"></script>
        <script src="/templates/make/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/templates/make/assets/global/plugins/backstretch/backstretch.min.js"></script>
        <script src="/templates/make/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="/templates/make/assets/global/js/pages/login-v2.js"></script>
      <script src="/templates/make/assets/admin/layout2/js/layout.js"></script>
  </body>
</html>
