<div class="col-lg-2 col-md-3 hidden-sm hidden-xs profil-right" >
{if $loggedin}
    <div class="profil-sidebar-element">
      <h3><strong>{$LANG.accountinfo}</strong></h3>
      <p><strong>{$LANG.clientareaphonenumber}:</strong> {$clientsdetails.phonenumber}</p>
      <p><strong>{$LANG.billingAddress}:</strong><br>{$clientsdetails.address1}</br>{$clientsdetails.postcode} {$clientsdetails.countryname}</p>
      <p><strong>{$LANG.clientareaemail}:</strong> {$clientsdetails.email}</p>

    </div>
    <div class="profil-sidebar-element m-t-20">
      <h3><strong>{$LANG.lastconn}</strong></h3>
      <p class="c-gray m-t-0"><i>{$clientsdetails.lastlogin}</i>
      </p>
     <!-- <h3><strong>AVERAGE RATING</strong></h3>
      <div id="stars" class="stars pull-left">
        <span class="fa fa-star c-primary"></span>
        <span class="fa fa-star c-primary"></span>
        <span class="fa fa-star c-primary"></span>
        <span class="fa fa-star c-primary"></span>
        <span class="fa fa-star-o c-primary"></span>
      </div>
      <div class="sidebar-number pull-right">4/5</div>-->
      <div class="clearfix"></div>
      <!--<h3><strong>MY STATS</strong></h3>
      <p class="m-t-0"><a href="/clientarea.php?action=services"><span class="c-primary"><strong>15</strong></span> Services</p></a>
      <p class="m-t-0"><a href="/clientarea.php?action=domains" class="c-primary"><span class="c-primary"><strong>8</strong></span> Domains</p></a>
      <p class="m-t-0"><a href="/supporttickets.php"><span class="c-primary"><strong>24</strong></span> Open Tickets</p></a>-->
    </div>
{/if}
    <div class="m-t-20">
	{if $loggedin}
      <a href="/clientarea.php?action=details">
        <button type="button" class="btn btn-block btn-primary bd-0 no-bd">{$LANG.manageyouraccount}</button>
      </a>
	  <a href="/clientarea.php?action=security">
        <button type="button" class="btn btn-block btn-primary bd-0 no-bd">{$LANG.clientareanavsecurity}</button>
      </a>
	  <a href="/clientarea.php?action=addfunds">
        <button type="button" class="btn btn-block btn-primary bd-0 no-bd">{$LANG.addfunds}</button>
      </a>
	 {else}
	 <div class="clearfix">
                            <div class="col-md-12">
                              <h4>First time customer?</h4>
                              <p>- 20% Discount</p>
							  <h4>Coupon Code</h4>
							  <p>NEWCUSTOMER</p>
                            </div>
                            
                          </div>
						  <br>
						  <br>
	 <a href="/login.php">
        <button type="button" class="btn btn-block btn-primary bd-0 no-bd"><i class="fa  fa-key"></i> &nbsp;Login</button>
      </a>
	  <a href="/register.php">
        <button type="button" class="btn btn-block btn-primary bd-0 no-bd"><i class="fa fa-user"></i> &nbsp;Register</button>
      </a>
	{/if}
    </div>
    <div class="m-t-60" style="width:100%">
      <canvas id="profil-chart" height="277" width="185" style="width: 185px; height: 277px;"></canvas>
    </div>
  </div>
  
  