<!DOCTYPE html>
{include file="$template/includes/tablelist.tpl" tableName="TicketsList" filterColumn="2"}
<script type="text/javascript">
  jQuery(document).ready( function ()
  {
    var table = jQuery('#tableTicketsList').removeClass('hidden').DataTable();
    {if $orderby == 'did' || $orderby == 'dept'}
    table.order(0, '{$sort}');
    {elseif $orderby == 'subject' || $orderby == 'title'}
    table.order(1, '{$sort}');
    {elseif $orderby == 'status'}
    table.order(2, '{$sort}');
    {elseif $orderby == 'lastreply'}
    table.order(3, '{$sort}');
    {/if}
    table.draw();
    jQuery('#tableLoading').addClass('hidden');
  });
</script>
<div class="page-content" style="margin-top: 21px; min-height:100vh;">
  <div class="col-lg-12 col-md-12">
    <div class="profil-content">
      <div class="header">
       <h2><strong>My</strong> Tickets</h2>
       <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          {foreach $breadcrumb as $item}
          <li{if $item@last} class="active"{/if}>
          {if !$item@last}<a href="{$item.link}">{/if}
          {$item.label}
          {if !$item@last}</a>{/if}
        </li>
        {/foreach}
      </ol>
    </div>
  </div>
	  <!--<div class="col-md-3">
        <div class="panel">
          <div class="panel-content text-center">
		  <h1><strong>Open</strong> Ticket</h1>
		  <p>Ask a question</p>
		  </div>
		</div>
	  </div>
	  <div class="col-md-3">
        <div class="panel">
          <div class="panel-content text-center">
		  <h1><strong>Visit</strong> Forum</h1>
		  <p>Ask a question</p>
		  </div>
		</div>
	  </div>
      <div class="col-md-6">
        <div class="panel">
          <div class="panel-content">
            <h1><strong>Search</strong> Our Knowledgebase</h1>
            <p><form action="https://community.phasehosting.io" target="_blank" method="get">
              <input type="text" name="s" class="form-control input-lg" placeholder="{$LANG.clientHomeSearchKb}" />
			  </form>
            </p>
          </div>
        </div>
      </div>-->
      <div class="col-md-4 col-md-offset-4 ">
        <a href="https://client.phasehosting.io/submitticket.php?step=2&deptid=1">
         <div class="panel-transparant">
           <div class="panel-content text-center">
            <i class="fa fa-plus fa-4x"></i>
            <h3>Open Ticket</h3>
          </div>
        </div>
      </a>
    </div>
    <!-- <div class="col-md-3">
      <a href="https://community.phasehosting.io/">
       <div class="panel-transparant">
         <div class="panel-content text-center">
          <i class="fa fa-user fa-4x"></i>
          <h3>Visit Community</h3>
        </div>
      </div>
    </a>
  </div> -->
  <div class="col-md-12">
    <div class="panel-transparant">
      <div class="panel-content">
        
        <table id="tableTicketsList" class="table table-list hidden">
          <thead>
            <tr>
              <th>{$LANG.supportticketsdepartment}</th>
              <th>{$LANG.supportticketssubject}</th>
              <th>{$LANG.supportticketsstatus}</th>
              <th>{$LANG.supportticketsticketlastupdated}</th>
            </tr>
          </thead>
          <tbody>
            {foreach from=$tickets item=ticket}
            <tr onclick="window.location='viewticket.php?tid={$ticket.tid}&amp;c={$ticket.c}'">
              <td class="text-">{$ticket.department}</td>
              <td><a href="viewticket.php?tid={$ticket.tid}&amp;c={$ticket.c}">{if $ticket.unread}<strong>{/if}#{$ticket.tid} - {$ticket.subject}{if $ticket.unread}</strong>{/if}</a></td>
              <td><span>{$ticket.status|strip_tags}</span></td>
              <td class="text-"><span class="hidden">{$ticket.normalisedLastReply}</span>{$ticket.lastreply}</td>
            </tr>
            {/foreach}
          </tbody>
        </table>
        <div class="text-center" id="tableLoading">
          <p><i class="fa fa-spinner fa-spin"></i> {$LANG.loading}</p>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>


