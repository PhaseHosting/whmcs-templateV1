<div class="page-content page-app page-profil" style="margin-top:21px; ">
  <div class="col-md-12">

    <div class="profil-content">
      <div class="header">
        <h2><strong>{$pagetitle}</strong></h2>
        <h2>{$displayTitle}</h2>
        <div class="breadcrumb-wrapper">
          <ol class="breadcrumb">
            {foreach $breadcrumb as $item}
            <li{if $item@last} class="active"{/if}>
            {if !$item@last}<a href="{$item.link}">{/if}
            {$item.label}
            {if !$item@last}</a>{/if}
          </li>
          {/foreach}
        </ol>
      </div>
    </div>
    <div class="panel-transparant" >
      <div class="panel-content" >
        {$text}
        <br />
        <br />
        <p>
          <strong>{$timestamp|date_format:"%A, %B %e, %Y"}</strong>
        </p>

        {if $googleplus1}
        <br />
        <br />
        <g:plusone annotation="inline"></g:plusone>
        {literal}<script type="text/javascript">
        (function() {
          var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
          po.src = 'https://apis.google.com/js/plusone.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
      </script>{/literal}
      {/if}

      {if $facebookrecommend}
      <br />
      <br />
      {literal}
      <div id="fb-root">
      </div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
      {/literal}
      <div class="fb-like" data-href="{$systemurl}{if $seofriendlyurls}announcements/{$id}/{$urlfriendlytitle}.html{else}announcements.php?id={$id}{/if}" data-send="true" data-width="450" data-show-faces="true" data-action="recommend">
      </div>
      {/if}

      {if $facebookcomments}
      <br />
      <br />
      {literal}
      <div id="fb-root">
      </div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
      {/literal}
      <fb:comments href="{$systemurl}{if $seofriendlyurls}announcements/{$id}/{$urlfriendlytitle}.html{else}announcements.php?id={$id}{/if}" num_posts="5" width="500"></fb:comments>
      {/if}
      <p>
        <a href="{$WEB_ROOT}/announcements.php" class="btn btn-default">{$LANG.clientareabacklink}</a>
      </p>
    </div>
  </div>
</div>
</div>
</div>
