<!DOCTYPE html>

{if !$invalidTicketId}
<div class="page-content" style="margin-top: 21px;">
  <div class="col-lg-12 col-md-12">
<div class="profil-content">
	<div class="header">
	<h2><strong>{$LANG.supportticketspagetitle}</strong> </h2>
	<h2>{$subject}</h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          {foreach $breadcrumb as $item}
          <li{if $item@last} class="active"{/if}>
          {if !$item@last}<a href="{$item.link}">{/if}
            {$item.label}
            {if !$item@last}</a>{/if}
          </li>
          {/foreach}
        </ol>
      </div>
    </div>
			{if $invalidTicketId}

    {include file="$template/includes/alert.tpl" type="danger" title=$LANG.thereisaproblem msg=$LANG.supportticketinvalid textcenter=true}

{else}

    {if $closedticket}
        {include file="$template/includes/alert.tpl" type="warning" msg=$LANG.supportticketclosedmsg textcenter=true}
    {/if}

    {if $errormessage}
        {include file="$template/includes/alert.tpl" type="error" errorshtml=$errormessage}
    {/if}

{/if}
			<div class="col-md-6">
    {foreach from=$descreplies key=num item=reply}


	
	
	<div class="panel-transparant">
	<div class="panel-content">
        <div class="ticket-reply{if $reply.admin} staff{/if}">
            <div class="panel-header">
                
				
                <h3>{$LANG.from} <strong><span class="sender" style="display: inline;">{$reply.name}
				
				</span></strong> • <span class="date" style="display: inline;">{$reply.date}</span></h3>
				
            </div>
		
            <div class="panel-content">
			<div class="{if $reply.admin}bd-red{else}bd-blue{/if}">
                             
                      <address>{$reply.message}</address>           
                            </div>
              
                {if $reply.id && $reply.admin && $ratingenabled}
                    <div class="clearfix">
                        {if $reply.rating}
                            <div class="rating-done">
                                {for $rating=1 to 5}
                                    <span class="star{if (5 - $reply.rating) < $rating} active{/if}"></span>
                                {/for}
                                <div class="rated">{$LANG.ticketreatinggiven}</div>
                            </div>
                        {else}
                            <div class="rating" ticketid="{$tid}" ticketkey="{$c}" ticketreplyid="{$reply.id}">
                                <span class="star" rate="5"></span>
                                <span class="star" rate="4"></span>
                                <span class="star" rate="3"></span>
                                <span class="star" rate="2"></span>
                                <span class="star" rate="1"></span>
                            </div>
                        {/if}
                    </div>
                {/if}
            </div>
            {if $reply.attachments}
                <div class="attachments">
                    <strong>{$LANG.supportticketsticketattachments} ({$reply.attachments|count})</strong>
                    <ul>
                        {foreach from=$reply.attachments key=num item=attachment}
                            <li><i class="fa fa-file-o"></i> <a href="dl.php?type={if $reply.id}ar&id={$reply.id}{else}a&id={$id}{/if}&i={$num}">{$attachment}</a></li>
                        {/foreach}
                    </ul>
                </div>
            {/if}
        </div>
		
		<span class="type">
                    
					
                </span>
		</div>
</div>

    {/foreach}
</div>
<div class="col-md-6" >
    <div class="panel-transparent panel-info panel-collapsable{if !$postingReply} panel-collapsed{/if} hidden-print" style="position:fixed;">
        <div class="panel-body{if !$postingReply} panel-body-collapsed{/if}">

            <form method="post" action="{$smarty.server.PHP_SELF}?tid={$tid}&amp;c={$c}&amp;postreply=true" enctype="multipart/form-data" role="form" id="frmReply">

                <div class="row">
					{if $loggedin}

					{else}
					<div class="form-group col-sm-6">
                        <label for="inputName">{$LANG.supportticketsclientname}</label>
						<input class="form-control form-white" type="text" name="replyname" id="inputName" value="{$replyname}" />
					</div>
					{/if}
					{if $loggedin}

					{else}
					<div class="form-group col-sm-6">
                        <label for="inputName">{$LANG.supportticketsclientemail}</label>
						<input class="form-control form-white" type="text" name="replyemail" id="inputEmail" value="{$replyemail}" />
					</div>
					{/if}
                    
                </div>

                <div class="form-group">
                    <label for="inputMessage">New {$LANG.contactmessage}</label>
                    <textarea name="replymessage" id="inputMessage" rows="12" class="form-control form-white">{$replymessage}</textarea>
                </div>

                <div class="row form-group">
                    <div class="col-sm-12">
                        <label for="inputAttachments">{$LANG.supportticketsticketattachments}</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="file" name="attachments[]" id="inputAttachments" class="form-control form-white" />
                        <div id="fileUploadsContainer"></div>
                    </div>
                    <div class="col-sm-3">
                        <button type="button" class="btn btn-default btn-block" onclick="extraTicketAttachment()">
                            <i class="fa fa-plus"></i> {$LANG.addmore}
                        </button>
                    </div>
                    <div class="col-xs-12 ticket-attachments-message text-muted">
                        {$LANG.supportticketsallowedextensions}: {$allowedfiletypes}
                    </div>
                </div>

                <div class="form-group text-center">
                    <input class="btn btn-primary" type="submit" name="save" value="{$LANG.supportticketsticketsubmit}" />
                    <input class="btn btn-default" type="reset" value="{$LANG.cancel}" onclick="jQuery('#ticketReply').click()" />
                </div>

            </form>

        </div>
    </div>
	</div>


{/if}
</div>
  </div>
</div>
