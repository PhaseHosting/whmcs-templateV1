<!DOCTYPE html>
<link href="/templates/make/assets/global/plugins/maps-amcharts/ammap/ammap.min.css" rel="stylesheet">
<link href="/templates/make/assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">

<!-- BEGIN PAGE CONTENT -->
<div class="page-content" style="margin-top: 21px;">

	<div class="header">
	<h2><strong>My</strong> Virtual Servers</h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          {foreach $breadcrumb as $item}
          <li{if $item@last} class="active"{/if}>
          {if !$item@last}<a href="{$item.link}">{/if}
            {$item.label}
            {if !$item@last}</a>{/if}
          </li>
          {/foreach}
        </ol>
      </div>
    </div>
	<div class="col-lg-12 col-md-12">
      <div class="profil-content">
	  
	 
		  
        <div class="panel">
          <div class="panel-header ">

            <div class="panel-content">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#overview" data-toggle="tab">Overview</a></li>
                {foreach $webHosting.products.product as $product}
                <li class=""><a href="#{$product.id}" data-toggle="tab">{$product.domain}</a></li>
                {/foreach}  
              </ul>
              <div class="tab-content">
                
                <div class="tab-pane fade active in" id="overview">
                  <div class="row column-seperation">
                    <div class="col-md-4 line-separator">
                      <h3><strong>{$LANG.manage}</strong> VPS</h3>
                      <h4>Control and manage your VPS instances</h4>
                      <p>&nbsp;</p>
                      <p>&nbsp;</p>
					  
					  <div class="text-center">
					  <a href="/ordervps.php" >
						<i class="fa fa-plus-circle fa-4x" style="text-align:center;"></i>
						<h3 style="text-align:center;"><strong>order</strong> new instance</h3>
					  </a>
					  </div>
					  
                      <p>&nbsp;</p>
                      <p>&nbsp;</p>
                      <p>&nbsp;</p>
                      <p>&nbsp;</p>
                      <p>&nbsp;</p>
                      <p>&nbsp;</p>
                    </div>
                    <div class="col-md-8">
                      <h3><strong>How-to:</strong> Quick start guide</h3>
                      
                    </div>
                      
                  </div>  
                </div>
                {foreach $webHosting.products.product as $product}
                <div class="tab-pane {if $product.first}fade active in{/if}" id="{$product.id}">
                  <div class="row column-seperation">
                    <div class="col-md-12">
                      <h3><strong>{$product.domain}</strong></a></h3>
                      
                      <div class="col-md-12">
					  {if $product.status eq Terminated}
					  <div class="col-md-12">
						<div class="text-center">
						<i class="fa fa-ban fa-4x"></i>
						<h4 class="text-center">Terminated</h4>
						</div>
					  </div>
					  {else}
					  <div class="col-md-4">
					  <p><strong>Registration date:</strong> {$product.regdate}</p>
					  <p><strong>Status:</strong> {$product.status}</p>
					  <p><strong>Pricing:</strong> {$product.recurringamount}</p>
					  </div>
					  <div class="col-md-8">
                        <a href="/clientarea.php?action=productdetails&id={$product.id}">
						<div class="col-md-4">
						<div class="text-center">
						<i class="fa fa-desktop fa-4x"></i>
						<h4 class="text-center">Manage VPS</h4>
						</div>
						</div>
					    </a>
					  
                        <a href="/upgrade.php?type=package&id={$product.id}">
						<div class="col-md-4">
						<div class="text-center">
						<i class="fa fa-arrow-circle-up fa-4x"></i>
						<h4 class="text-center">Upgrade Package</h4>
						</div>
						</div>
					    </a>
					  
						<a href="/clientarea.php?action=cancel&id={$product.id}">
						<div class="col-md-4">
						<div class="text-center">
						<i class="fa fa-ban fa-4x"></i>
						<h4 class="text-center">Request Cancellation</h4>
						</div>
						</div>
						</a>
						</div>
                        {/if}
                      
					</div>
                     
                      </div>
                      <div class="col-md-6">
                        <!--<p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down). default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                        <p class="light text-right">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                        <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                        -->
                      </div>
                    </div>
                  </div>
                  {/foreach} 
                  
                </div>
              </div>
            </div>
          </div>
		  
	
        </div> 
      </div>
    </div>

</div>

<script src="/templates/make/assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script> <!-- Inline Edition X-editable -->
<script src="/templates/make/assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script> <!-- Context Menu -->
<script src="/templates/make/assets/global/plugins/multidatepicker/multidatespicker.min.js"></script> <!-- Multi dates Picker -->
<script src="/templates/make/assets/global/js/widgets/todo_list.js"></script>
<script src="/templates/make/assets/global/plugins/metrojs/metrojs.min.js"></script> <!-- Flipping Panel -->
<script src="/templates/make/assets/global/plugins/charts-chartjs/Chart.min.js"></script>  <!-- ChartJS Chart -->
<script src="/templates/make/assets/global/plugins/charts-highstock/js/highstock.min.js"></script> <!-- financial Charts -->
<script src="/templates/make/assets/global/plugins/charts-highstock/js/modules/exporting.min.js"></script> <!-- Financial Charts Export Tool -->
<script src="/templates/make/assets/global/plugins/maps-amcharts/ammap/ammap.min.js"></script> <!-- Vector Map -->
<script src="/templates/make/assets/global/plugins/maps-amcharts/ammap/maps/js/worldLow.min.js"></script> <!-- Vector World Map  -->
<script src="/templates/make/assets/global/plugins/maps-amcharts/ammap/themes/black.min.js"></script> <!-- Vector Map Black Theme -->
<script src="/templates/make/assets/global/plugins/skycons/skycons.min.js"></script> <!-- Animated Weather Icons -->
<script src="/templates/make/assets/global/plugins/simple-weather/jquery.simpleWeather.js"></script> <!-- Weather Plugin -->
<script src="/templates/make/assets/global/js/widgets/widget_weather.js"></script>
<script src="/templates/make/assets/global/js/pages/dashboard.js"></script>