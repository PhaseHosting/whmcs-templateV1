<!DOCTYPE html>
<link href="/templates/make/assets/global/plugins/maps-amcharts/ammap/ammap.min.css" rel="stylesheet">
<link href="/templates/make/assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">

<!-- BEGIN PAGE CONTENT -->
<div class="page-content" style="margin-top: 21px;">
  
    
	<div class="header">
	<h2><strong>My</strong> WordPress Websites</h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          {foreach $breadcrumb as $item}
          <li{if $item@last} class="active"{/if}>
          {if !$item@last}<a href="{$item.link}">{/if}
            {$item.label}
            {if !$item@last}</a>{/if}
          </li>
          {/foreach}
        </ol>
      </div>
    </div>    
    <div class="col-lg-12 col-md-12">
      <div class="profil-content">
        <div class="panel">
          <div class="panel-header ">
            <div class="panel-content">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#overview" data-toggle="tab">Overview</a></li>
                {foreach $hostname.products.product as $product}
                <li class=""><a href="#{$product.id}" data-toggle="tab">{$product.domain}</a></li>
                {/foreach}  
              </ul>
              <div class="tab-content">
                
                <div class="tab-pane fade active in" id="overview">
                 <div class="row column-seperation">
                    <div class="col-md-4 line-separator">
                    <h3><strong>{$LANG.manage}</strong> {$LANG.affiliateshostingpackage}</h3>
                    <h4>Administrate and customize your websire</h4>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <div class="text-center">
					  <a href="/cart.php?gid=2?carttpl=make" >
						<i class="fa fa-plus-circle fa-4x" style="text-align:center;"></i>
						<h3 style="text-align:center;"><strong>order</strong> WordPress</h3>
					  </a>
					  </div>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                  </div>
                  <div class="col-md-7">
                    <!--<h3><strong>How-to:</strong> Quick start guide</h3>
                    <a href="#createmail" data-toggle="tab"><h4>Create an email address</h4></a>
                    <p class="light" data-toggle="tab">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                    <a href="#upload" data-toggle="tab"><h4>Upload your website</h4></a>
                    <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                    <a href="#ftp" data-toggle="tab"><h4>Connect with a FTP client</h4></a>
                    <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                    -->
                 </div> 
                  </div>  
                </div>
				
				  {foreach $hostname.products.product as $product}
                <div class="tab-pane {if $product.first}fade active in{/if}" id="{$product.id}">
                  <div class="row column-seperation">
                    <div class="col-md-12">
                      <h3><strong>{$product.domain}</strong></a></h3>
                      
                      <div class="col-md-12">
					  {if $product.status eq Terminated}
					  <div class="col-md-12">
						<div class="text-center">
						<i class="fa fa-ban fa-4x"></i>
						<h4 class="text-center">Terminated</h4>
						</div>
					  </div>
					  {else}
					  <div class="col-md-4">
					  <p><strong>Registration date:</strong> {$product.regdate}</p>
					  <p><strong>Status:</strong> {$product.status}</p>
					  <p><strong>Pricing:</strong> {$product.recurringamount}</p>
					  </div>
					  <div class="col-md-8">
                        <a href="/clientarea.php?action=productdetails&id={$product.id}">
						<div class="col-md-4">
						<div class="text-center">
						<i class="fa fa-desktop fa-4x"></i>
						<h4 class="text-center">Manage WordPress</h4>
						</div>
						</div>
					    </a>
					  
                        <a href="/upgrade.php?type=package&id={$product.id}">
						<div class="col-md-4">
						<div class="text-center">
						<i class="fa fa-arrow-circle-up fa-4x"></i>
						<h4 class="text-center">Upgrade Package</h4>
						</div>
						</div>
					    </a>
					  
						<a href="/clientarea.php?action=cancel&id={$product.id}">
						<div class="col-md-4">
						<div class="text-center">
						<i class="fa fa-ban fa-4x"></i>
						<h4 class="text-center">Request Cancellation</h4>
						</div>
						</div>
						</a>
						</div>
                        {/if}
                      
					</div>
                     
                      </div>
                      <div class="col-md-6">
                        <!--<p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down). default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                        <p class="light text-right">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                        <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                        -->
                      </div>
                    </div>
                  </div>
                  {/foreach}
                  
				  <div class="tab-pane " id="createmail">
                  <div class="row column-seperation">
                    <div class="col-md-8 line-separator">
                    <h3><strong>Create</strong> an email address</h3>
					<p></p>
                    <h4>Step 01</h4>
                    <p>Click on your website above</p>
					<h4>Step 02</h4>
                    <p>select Configure Website</p>
					<h4>Step 03</h4>
                    <p>Scroll down and find the create email section</p>
					<h4>Step 04</h4>
                    <p>Enter your desired email and provide a strong password</p>
					<h4>Step 05</h4>
                    <p>Click on create. </p>
					<p>Now you can login to your webmail through yourdomain.com/webmail </p>
                    
                  </div>
                  <div class="col-md-4">
                    <h3><strong>How-to:</strong> Quick start guide</h3>
                    <a href="#createmail"><h4>Create an email address</h4></a>
                    <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                    <a href="#upload"><h4>Upload your website</h4></a>
                    <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                    <a href="#ftp"><h4>Connect with a FTP client</h4></a>
                    <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                    
                 </div> 
                  </div>  
                </div>
				<div class="tab-pane " id="upload">
                  <div class="row column-seperation">
                    <div class="col-md-8 line-separator">
                    <h3><strong>Upload</strong> your website</h3>
					<p></p>
                    <h4>Step 01</h4>
                    <p>Click on your website above</p>
					<h4>Step 02</h4>
                    <p>select Configure Website</p>
					<h4>Step 03</h4>
                    <p>Click on File Manager</p>
					<h4>Step 04</h4>
                    <p>From here you can easily upload and manage your files</p>
					
                    
                  </div>
                  <div class="col-md-4">
                    <h3><strong>How-to:</strong> Quick start guide</h3>
                    <a href="#createmail"><h4>Create an email address</h4></a>
                    <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                    <a href="#upload"><h4>Upload your website</h4></a>
                    <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                    <a href="#ftp"><h4>Connect with a FTP client</h4></a>
                    <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                    
                 </div> 
                  </div>  
                </div>
				<div class="tab-pane " id="ftp">
                  <div class="row column-seperation">
                    <div class="col-md-8 line-separator">
                    <h3><strong>Connect</strong> with a FTP client</h3>
                    <p></p>
                    <h4>Step 01</h4>
                    <p>Download and install a FTP client for your desktop.<br>
					We recomend that you use <a href="https://filezilla-project.org/download.php" target="_blank">FileZilla</a></p>
					<h4>Step 02</h4>
                    <p>Fill in your domain name as host<br>Fill in your cPanel username and password</p>
					<h4>Step 03</h4>
                    <p>For a secure (Encrypted) connection use port 22 to connect</p>
					<h4>Step 04</h4>
                    <p>From here you can easily upload and manage your files</p>
                    
                  </div>
                  <div class="col-md-4">
                    <h3><strong>How-to:</strong> Quick start guide</h3>
                    <a href="#createmail"><h4>Create an email address</h4></a>
                    <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                    <a href="#upload"><h4>Upload your website</h4></a>
                    <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                    <a href="#ftp"><h4>Connect with a FTP client</h4></a>
                    <p class="light">default, the textarea element comes with a vertical scrollbar (and maybe even a horizontal scrollbar). This vertical scrollbar enables the user to continue entering and reviewing their text (by scrolling up and down).</p>
                    
                 </div> 
                  </div>  
                </div>
				  
                </div>
              </div>
            </div>
          </div>
          
        </div> 
      </div>
    </div>

</div>

<script src="/templates/make/assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script> <!-- Inline Edition X-editable -->
<script src="/templates/make/assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script> <!-- Context Menu -->
<script src="/templates/make/assets/global/plugins/multidatepicker/multidatespicker.min.js"></script> <!-- Multi dates Picker -->
<script src="/templates/make/assets/global/js/widgets/todo_list.js"></script>
<script src="/templates/make/assets/global/plugins/metrojs/metrojs.min.js"></script> <!-- Flipping Panel -->
<script src="/templates/make/assets/global/plugins/charts-chartjs/Chart.min.js"></script>  <!-- ChartJS Chart -->
<script src="/templates/make/assets/global/plugins/charts-highstock/js/highstock.min.js"></script> <!-- financial Charts -->
<script src="/templates/make/assets/global/plugins/charts-highstock/js/modules/exporting.min.js"></script> <!-- Financial Charts Export Tool -->
<script src="/templates/make/assets/global/plugins/maps-amcharts/ammap/ammap.min.js"></script> <!-- Vector Map -->
<script src="/templates/make/assets/global/plugins/maps-amcharts/ammap/maps/js/worldLow.min.js"></script> <!-- Vector World Map  -->
<script src="/templates/make/assets/global/plugins/maps-amcharts/ammap/themes/black.min.js"></script> <!-- Vector Map Black Theme -->
<script src="/templates/make/assets/global/plugins/skycons/skycons.min.js"></script> <!-- Animated Weather Icons -->
<script src="/templates/make/assets/global/plugins/simple-weather/jquery.simpleWeather.js"></script> <!-- Weather Plugin -->
<script src="/templates/make/assets/global/js/widgets/widget_weather.js"></script>
<script src="/templates/make/assets/global/js/pages/dashboard.js"></script>